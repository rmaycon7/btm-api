// import app from "./src/app";
// import chalk from "chalk";

const app = require("../src/app/app"),
	chalk = require("chalk"),
	http = require("http");
let {
	nextAvailable
} = require('node-port-check');
require('dotenv').config({
	path: 'config.env'
})
// setInterval(() => {
//     http.get("https://btm-api.herokuapp.com");
// }, 126000000); 

// console.log();
let argv = require('minimist')(process.argv.slice(2));
console.dir(argv);

let port = argv.port || argv.PORT
console.log(port)

const getport = (porta = port || 8080) => process.env.PORT || porta,
	os = require('os')

nextAvailable(getport(port), '0.0.0.0').then((next) => {
	let tp = Object.entries(os.networkInterfaces())
	let ips = ""
	for (let i = 0; i < tp.length; i++) {

		ips += "http://" + tp[i][1][0].address + ":" + next
		if (i < tp.length - 1) {
			ips += ", "
		}
	}
	app.listen(next, () => {
		// console.log(port());
		console.clear();
		// console.log("\x1b[36m%s\x1b[0m", "I am cyan");
		console.log(chalk.red("\t\tWellcome To API resources"));
		console.log(chalk.blue("\t\t\tDevelopment mode!"));

		console.log(chalk.green("\tServer running in " + ips));
	});
})


// let nodemon = require('nodemon');

// nodemon({
// 	script: './dev/index.js'
// }).on('start', function() {
// 	console.log(Date())
// 	console.log('nodemon started');
// }).on('exit', function(files) {
// 	console.log('script crashed for some reason');
// 	nodemon.emit('restart');
// 	nodemon.emit('quit');
// });

// force a restart

// {
//     errors:[
//         {
//             code: 
//         }
//     ]
// }