### Rotas da API

Online => [https://api-dev-api.193b.starter-ca-central-1.openshiftapps.com](https://api-dev-api.193b.starter-ca-central-1.openshiftapps.com)

User:

    get /users/ // listar todos os usuários
    get /users/:userId // lista apenas um usuário
    post /users/ // criar um usuário
    patch /users/:userId // atualizar um usuário
    delete /users/:userId // deletar um usuário

UserGroups: 

    get /users/invitations/ // listar todos os convites do para entra em grupos que o usuário possuí
    post /users/invitations/:inviteId // confirmar para entra em grupo grupo
    delete /users/invitations/:inviteId // recusar convite para entra em um grupo
    delete /users/group/:groups/:groupId // sair de um grupo 

User Reports:

    get /report/users/weekdays // viazualizar a quantidade de usuário criada em cada dia da semana

Group:

    get /groups/ listar todos os grupos
    get /groups/:groupId // viazualizar apenas um grupo
    post /groups/ // criar um grupo
    patch /groups/:groupId // atualizar um grupo
    delete /groups/:groupId // deletar um grupo
    

Group Reports:

    get /report/groups/weekdays // viazualizar a quantidade de grupos criada em cada dia da semana


Group Members:

    get /groups/:groupId/members/   //  listar membros do grupo
    post /groups/:groupId/members  //  adicionar membro no grupo
    delete /groups/:groupId/members/:memberId    //  remover membro do grupo


Group Categories:

    get /groups/:groupId/categories     //listar todas as categorias de um grupo
    get /groups/:groupId/categories/:categoryId     // ver apenas uma categoria de um grupo
    post    /groups/:groupId/categories     //  criar uma categoria de um grupo
    patch   /groups/:groupId/categories/:categoryId     // atualizar uma  categoria de um grupo
    delete  /groups/:groupId/categories/:categoryId     //  deletar uma categoria de um grupo


Category Tasks:

    get /groups/:groupId/categories/:categoryId/:groupId/tasks/         //  Listar todas as tarefas da categoria
    get /groups/:groupId/categories/:categoryId/:groupId/tasks/:taskId          //  Ver uma tarefa especifica da categoria
    post /groups/:groupId/categories/:categoryId/:groupId/tasks/            //  Criar uma tarefa na categoria
    patch /groups/:groupId/categories/:categoryId/:groupId/tasks/:taskId            //  Atualizar uma tarefa na categoria
    delete /groups/:groupId/categories/:categoryId/:groupId/tasks/:taskId           //  deletar uma tarefa na categoria




