# Documentação da API
## Conteúdo

1. [Segurança](#seguran-a)
2. [Tipo de dado suportado](#tipo-de-dado-suportado)
3. [Métodos Suportados](#m-todos-suportados)
4. [Exemplos](#exemplos-)
	* [Cadastro](#cadastro)
5. [Rotas e Models](#rotas-e-models)
	* [Grupos](#grupos)
	* [Grupos - Membros](#grupo-membros)
	* [Grupos - Notas](#grupo-notas)
	* [Grupos - Categorias](#grupo-categorias)
	* [Grupos - Categorias - Tarefas](#grupo-categorias-tarefas)
	* [Usuário](#usuário)


API provedora de recursos para a aplciação BTM (Best Task Manager)

<span style="color: #366DB9" >Link para a API online => </span> [btm-pi](https://api-dev-api.193b.starter-ca-central-1.openshiftapps.com)

### Segurança

A aplicação usa segurança [JWT](https://jwt.io), um token é gerado a cada vez que o usúario se cadastra ou loga no sistema, ao acessar os recursos protegidos do systema o token deve ser envia no cabeçalho da requisição, exemplo: 
<span style="color: #FF9C00">
	
~~~http
Authorization: Bearer "token"
~~~ 

</span>
Todas as rotas da API requerm o token de acsso, exceto <span style="color: #3C50CE">/authenticate</span>  e <span style="color: #3C50CE"> /register </span>, que são rotas usadas para criar uma nova conta ou logar em uma conta já existente. 

### Tipo de dado suportado
A API so trabalha como dados to tipo JSON, exemplo:
~~~http
Content-Type: application/json
~~~
### Métodos suportados
~~~http
POST, PATCH, GET, DELETE, OPTIONS
~~~

### Rotas e Models
#### Usuário
##### Model
###### Usuário - Model

Um usuário possuí 

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome do usuário",
	"email": "email do usuário",
}
~~~

###### Usuário - Rotas e seus retornos 

**Visualizar** dados do usuário.

~~~http 
get /users/:userId 	
~~~
userId = id do usuário
###### Sucesso

Retorna um objeto json contendo os dados do grupo que o usúario é o dono ou apenas um membro.

~~~json
[
	
	{
		"id": "5b46c8ea06b8d5003a6133dc",
		"name": "Nome usuário",
		"email": "email do usuário"
	}
]
~~~ 

###### Usuário não encontrado

Caso o usuário seje encontrado para o usuário.

[Exemplo => Erro](#erro-22)

**Atualizar** conta do usuário.

~~~http 
patch /users/:userId 	
~~~
corpo da requisição:
~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome do usuário",
	"email": "Email do usúario"
}
~~~
###### Sucesso

Retorna um objeto json contendo todos os dados do usúario.

~~~json

{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome do usuário",
	"email": "Email do usúario"
}

~~~ 
###### Usuário não encontrado

Caso o usuário seje encontrado.

[Exemplo => Erro](#erro-22)

**Deletar** conta  do usúario.

~~~http 
delete /users/:userId 	
~~~

###### Sucesso

[Exemplo => Sucesso](#sucesso-exemplo)

###### Erro

Usuário não encontrado

[Exemplo => Erro](#erro-22)




**Visualizar** convites

~~~http
get /users/invitations
~~~
###### sucesso

Retornado um array de objeto json contendo todos os convites recebidos

~~~json
[
	{
		"id": "5b46c8ea06b8d5003a6133dc", 	// Id do grupo que o usuário foi convidado a entrar 
		"name": "Nome do grupo",
		"description": "Descrição do grupo"
	}
]
~~~

###### Erro
[Exemplo => erro](#erro-22)

**Confirmar** convites

~~~http
post /users/invitations/:inviteId
~~~
inviteId = Id do convite que o usúario deseja aceitar

###### Sucesso

[Exemplo => sucesso](#sucesso-exemplo)

###### Erro

[Exemplo => erro](#erro-22)

**recusar** convites

~~~http
delete /users/invitations/:inviteId
~~~

inviteId = Id do convite que o usúario deseja aceitar

###### Sucesso

[Exemplo => sucesso](#sucesso-exemplo)

###### Erro

[Exemplo => erro](#erro-22)

#### Grupos
##### Model
###### Grupo - Model

Um grupo possuí 
~~~json
{
	"name": "Nome",
	"description": "Descrição",
	"owner": "Dono",
	"members": "Mebros",
	"categories": "Categorias",
	"notes": "Notas"
}
~~~

###### Grupo - Rotas e seus retornos 

**Vizualizar** todos os grupos do usuário

~~~http
get /groups
~~~
###### Sucesso
Retorna um array de objetos json contendo todos os dados dos grupos que o usúario é o dono ou apenas um membro.

~~~json
[
	
	{
		"id": "5b46c8ea06b8d5003a6133dc",
		"name": "Nome",
		"description": "Descrição",
		"isOwner": true,	// true se o usuário for o dono do grupo e false se não for
	}
]
~~~ 
###### Nenhum grupo encontrado

Caso nenhum grupo seje encontrado para o usuário.

[Exemplo => Erro](#erro-22)

**Visualizar** apenas um grupo do usuário.

~~~http 
get /groups/:grouId 	
~~~
grouId = id do grupo
###### Sucesso
Retorna um objeto json contendo os dados do grupo que o usúario é o dono ou apenas um membro.

~~~json
[
	
	{
		"id": "5b46c8ea06b8d5003a6133dc",
		"name": "Nome",
		"description": "Descrição",
		"isOwner": true,	// true se o usuário for o dono do grupo e false se não for
	}
]
~~~ 
###### Grupo não encontrado

Caso o grupo seje encontrado para o usuário.

[Exemplo => Erro](#erro-22)

**Criar** um grupo.

~~~http 
post /groups/ 	
~~~
corpo da requisição:
~~~json
{
	"name": "Nome do grupo",
	"description": "Descrição do grupo"
}
~~~
###### Sucesso
Retorna um objeto json contendo todos os grupos que o usúario é o dono ou apenas um membro.

~~~json
	
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome",
	"description": "Descrição",
	"isOwner": true,	// true se o usuário for o dono do grupo e false se não for
}

~~~ 
###### Grupo não encontrado

Caso o grupo seje encontrado para o usuário.

[Exemplo => Erro](#erro-22)

**Atualizar** um grupo do usuário.

~~~http 
patch /groups/:grouId 	
~~~
corpo da requisição:
~~~json
{
	"name": "Nome do grupo",
	"description": "Descrição do grupo"
}
~~~
###### Sucesso

Retorna um objeto json contendo todos os grupos que o usúario é o dono ou apenas um membro.

~~~json

{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome",
	"description": "Descrição",
	"isOwner": true,	// true se o usuário for o dono do grupo e false se não for
}

~~~ 
###### Grupo não encontrado

Caso o grupo seje encontrado para o usuário.

[Exemplo => Erro](#erro-22)

**Deletar** um grupo  do usúario.

~~~http 
delete /groups/:grouId 	
~~~

###### Sucesso

[Exemplo => Sucesso](#sucesso-exemplo)

###### Erro

Grupo não encontrado

[Exemplo => Erro](#erro-22)

#### Grupo - Membros
##### Model
###### Membro - Model

Um é um usuário

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome",
	"email": "email do usúario",
}
~~~

###### Grupos - Membros - Rotas e seus retornos 

**Listar** membros do grupo

~~~http
get /groups/:grouId/members
~~~

###### Sucesso

Retorna um array de objetos json com os dados dos membros do grupo

~~~json
[
	{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome do mebro",
	"email": "Email do membro"
	}
]
~~~

###### Grupo não encontrado

Caso o grupo seje encontrado para o usuário.

[Exemplo => Erro](#erro-22)

###### Erro

Caso o Nenhum mebro seja encontrado 

[Exemplo => Erro](#erro-22)


**Adicionar membro** ao grupo

~~~http
post /groups/:grouId/members
~~~

Corpo da requisição

~~~json
{
	"email": "email do usuário que deseja adicionar ao grupo"
}
~~~

###### Sucesso

Caso o convite seja enviado com sucesso

[Exemplo => sucesso](#sucesso-exemplo)

###### Erro

Caso o membro não seja encontrado

[Exemplo => Erro](#erro-22)


**Remover membro** do grupo

~~~http
delete /groups/:grouId/members/:memberId
~~~

###### Sucesso

Caso o membro seja removido com sucesso

[Exemplo => sucesso](#sucesso-exemplo)

###### Erro

Caso o membro não seja encontrado ou o usuário não tenha autorização para efetuar esta operação

[Exemplo => Erro](#erro-22)


#### Grupo - Notas
##### Model
###### Nota - Model

É um exemplo do conteúdo de uma nota

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome",
	"contente": "Descrição/conteúdo",
}
~~~

###### Grupos - Notas - Rotas e seus retornos 


**Vizualizar** todas as notas do grupo

~~~http
get /groups/:grouId/notes
~~~

###### Sucesso

É retorndao um array de objetos json contendo todos os dados das notas do grupo.

~~~json
[
	{
		"id": "5b46c8ea06b8d5003a6133dc",
		"name": "nome da nota",
		"content": "Coteúdo da nota",
		"owner": {
			"id": "5b46c8ea06b8d5003a6133dc",
			"name": "nome do usuário que criou a nota",
			"email": "email do usúario que criou a nota"
		}
	}
]
~~~

###### Erro

Caso a nota ou grupo nãos seja encontrado

[Exemplo => erro](#erro-22)

**Visualizar** uma nota

~~~http
get /groups/:grouId/notes/:noteId
~~~

###### Sucesso

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "nome da nota",
	"content": "Coteúdo da nota",
	"owner": {
		"id": "5b46c8ea06b8d5003a6133dc",
		"name": "nome do usuário que criou a nota",
		"email": "email do usúario que criou a nota"
	}
}
~~~

###### Erro

Caso aja erro ou o grupo ou a nota não seja encontrada

[Exemplo => erro](#erro-22)

**Criar** nota

~~~http
post /groups/:grouId/notes
~~~

Corpo da requisição

~~~json
{
	"name": "nome da nota",
	"content": "Coteúdo da nota"
}
~~~

###### Sucesso

Será retornado um objeto json 

~~~json
{
	"name": "nome da nota",
	"content": "Coteúdo da nota"
}
~~~

###### Erro

Se houver algum erro ou o grupo não encontrado

[Exemplo => erro](#erro-22)


**Atualizar** nota

~~~http
patch /groups/:grouId/notes/:noteId
~~~

Corpo da requisição

~~~json
{
	"name": "nome da nota",
	"content": "Coteúdo da nota"
}
~~~

###### Sucesso

Será retornado um objeto json 

~~~json
{
	"name": "nome da nota",
	"content": "Coteúdo da nota"
}
~~~

###### Erro

Se houver algum erro ou o grupo não encontrado ou mesmo o usuário não tenha autorização

[Exemplo => erro](#erro-22)

**Deletar** nota

~~~http
delete /groups/:grouId/notes/:noteId
~~~

###### Sucesso
Caso a operação seja executada com sucesso é retornada uma mensagem informando o sucesso.

[Exemplo => sucesso](#sucesso-exemplo)

###### Deletar

Se houver algum erro ou o grupo não encontrado ou mesmo o usuário não tenha autorização

[Exemplo => erro](#erro-22)


#### Grupo - Categorias
##### Model
###### Categoria - Model

É um exemplo do conteúdo de uma categoria

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome",
	"description": "Descrição/conteúdo",
}
~~~

###### Grupos - categorias - Rotas e seus retornos 


**Vizualizar** todas as categorias do grupo

~~~http
get /groups/:grouId/categories
~~~

###### Sucesso

Sera retornado um array de objetos json com os dados de todas as categorias

~~~json
[
	{
		"id": "5b46c8ea06b8d5003a6133dc",
		"name": "nome da categoria",
		"description": "Coteúdo da categoria"
	}
]
~~~

###### Erro

Caso a nota ou grupo nãos seja encontrado

[Exemplo => erro](#erro-22)

**Visualizar** uma categoria

~~~http
get /groups/:grouId/categories/:categoryId
~~~

###### Sucesso

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "nome da categoria",
	"description": "Coteúdo da categoria",
	"owner": {
		"id": "5b46c8ea06b8d5003a6133dc",
		"name": "nome do usuário que criou a categoria",
		"email": "email do usúario que criou a categoria"
	}
}
~~~

###### Erro

Caso aja erro ou o grupo ou a categoria não seja encontrada

[Exemplo => erro](#erro-22)

**Criar** categoria

~~~http
post /groups/:grouId/categories
~~~

Corpo da requisição

~~~json
{
	"name": "nome da categoria",
	"description": "Coteúdo da categoria"
}
~~~

###### Sucesso

Será retornado um objeto json 

~~~json
{
	"name": "nome da categoria",
	"description": "Coteúdo da categoria"
}
~~~

###### Erro

Se houver algum erro ou o grupo não encontrado

[Exemplo => erro](#erro-22)


**Atualizar** categoria

~~~http
patch /groups/:grouId/categories/:categoryId
~~~

Corpo da requisição

~~~json
{
	"name": "nome da categoria",
	"description": "Coteúdo da categoria"
}
~~~

###### Sucesso

Será retornado um objeto json 

~~~json
{
	"name": "nome da categoria",
	"description": "Coteúdo da categoria"
}
~~~

###### Erro

Se houver algum erro ou o grupo não encontrado ou mesmo o usuário não tenha autorização

[Exemplo => erro](#erro-22)

**Deletar** categoria

~~~http
delete /groups/:grouId/categories/:categoryId
~~~

###### Sucesso
Caso a operação seja executada com sucesso é retornada uma mensagem informando o sucesso.

[Exemplo => sucesso](#sucesso-exemplo)

###### Erro

Se houver algum erro ou o grupo não encontrado ou mesmo o usuário não tenha autorização

[Exemplo => erro](#erro-22)



#### Grupo - Categorias - Tarefas
##### Model
###### Tarefa - Model

É um exemplo do conteúdo de uma tarefa

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "Nome",
	"description": "Descrição/conteúdo",
	"private": true		// true se a tarefa for privada e false caso não seja
}
~~~

###### Grupos - Categorias - Tarefas - Rotas e seus retornos 


**Vizualizar** todas as tarefas de uma categoria do grupo

~~~http
get /groups/:grouId/categories/tasks
~~~

###### Sucesso

Sera retornado um array de objetos json com os dados de todas as tarefas de uma categoria

~~~json
[
	{
		"id": "5b46c8ea06b8d5003a6133dc",
		"name": "nome da tarefa",
		"description": "Descrição da tarefa",
		"private": true
	}
]
~~~

###### Erro

Caso a nota ou grupo nãos seja encontrado

[Exemplo => erro](#erro-22)

**Visualizar** uma tarefa

~~~http
get /groups/:grouId/categories/:categoryId/tasks/taskId
~~~

###### Sucesso

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "nome da tarefa",
	"description": "Descrição da tarefa",
	"private": true
}
~~~

###### Erro

Caso aja erro ou o grupo ou a tarefa não seja encontrada

[Exemplo => erro](#erro-22)

**Criar** tarefa

~~~http
post /groups/:grouId/categories/tasks
~~~

Corpo da requisição

~~~json
{
	"name": "nome da tarefa",
	"description": "Descrição da tarefa",
	"private": true
}
~~~

###### Sucesso

Será retornado um objeto json 

~~~json
{	

	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "nome da tarefa",
	"description": "Descrição da tarefa",
	"private": true,
	"users": [
		{
			"userId": "5b46c8ea06b8d5003a6133dc"	// id do usuário
		}
	]
}
~~~
users = usuários é um campo opicional, ele contém todos os usuário que são responsaveis pela tarefa, deve ser infrmado o id do usuáio

###### Erro

Se houver algum erro ou o grupo não encontrado

[Exemplo => erro](#erro-22)


**Atualizar** tarefa

~~~http
patch /groups/:grouId/categories/:categoryId/tasks/taskId
~~~

Corpo da requisição

~~~json
{

	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "nome da tarefa",
	"description": "Descrição da tarefa",
	"private": true,
	"users": [
		{
			"userId": "5b46c8ea06b8d5003a6133dc"	// id do usuário
		}
	]
}
~~~
users = usuários é um campo opicional, ele contém todos os usuário que são responsaveis pela tarefa, deve ser infrmado o id do usuáio

###### Sucesso

Será retornado um objeto json 

~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",
	"name": "nome da tarefa",
	"description": "Descrição da tarefa",
	"private": true
}
~~~

###### Erro

Se houver algum erro ou o grupo não encontrado ou mesmo o usuário não tenha autorização

[Exemplo => erro](#erro-22)

**Deletar** tarefa

~~~http
delete /groups/:grouId/categories/:categoryId/tasks/taskId
~~~

###### Sucesso
Caso a operação seja executada com sucesso é retornada uma mensagem informando o sucesso.

[Exemplo => sucesso](#sucesso-exemplo)

###### Erro

Se houver algum erro ou o grupo não encontrado ou mesmo o usuário não tenha autorização

[Exemplo => erro](#erro-22)



### Exemplos: 
~~~json
{
	"age": 23,
	"name": "Alan",
	"occupation": "Student"
}
~~~
#### Cadastro
Nesta API um usuário poderá ter acesso aos recursos se cadastrando ou fazendo login informando os seguintes dados para a rota /register utilizando o método POST:

~~~json
{
	"email": "'email do usuário'",	// os email são únicos, não se repetem,
	"name": "'Nome completo do usuário'",
	"password": "'senha que o usuário quiser'"
}
~~~
#### Retorno 
Caso o cadastro seja efetuado com sucesso, seram retornado os seguintes dados:
~~~json
{
	"id": "5b46c8ea06b8d5003a6133dc",	// Um id único que identifica o usuário
	"email": "'email cadastrado'",
	"name": "'Nome do usuário'",
	"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViNDZjOGVhMDZiOGQ1MDAzYTYxMzNkYyIsImlhdCI6MTUzMTM2NTYxMCwiZXhwIjoxNTMxNDUyMDEwfQ.EOtQ_8CB6bRog2TxnuGYNdaA5LU9m_5SV2JLO7jvHu0"	//Token JWT único de acesso, usado para acessar todos os dados que o usúario tem/pode ter acesso.
}
~~~
##### Sucesso Exemplo

Deletar dados, deletar um grupo por exemplo.

~~~json
{
	"code": "GROUP_DELETED",
	"message": "Grupo deletado com sucesso.",
	"name": "Deletar Grupo",
	"statusCode": 202
}
~~~

##### Erro
Em caso de erro é retornado um array de objetos contendo todos os erros, exemplo:

<span style="color: red">(Obervação)</span> O statusCude também é retornado no cabeçalho da resposta http.

~~~json
{
	"error": [
		{
			"statusCode": 409,	// é o status http da requisição
			"name": "Cadastrar usuárioo",	// nome do erro
			"message": "Usuário já existe, mude o email e tente novamente.",	// mensagem exlpicando qual foi o erro
			"code": "USER_EXISTS"	// código do erro
		}
	]
}
~~~
se o erro for falta de algum campo na requisição, a api também retornará os camplos que são obrigatórios o seu envio na hora de cadastrar o dado, exemplo na hora do usuário se cadastrar:

~~~json
{
	"error": [
		{
			"statusCode": 428,
			"name": "Error",
			"message": "Estes campos são obrigatórios, preencha os campos e tente novamente.",
			"fields": [
				"name",
				"email",
				"password"
			],
			"code": "FIELDS_NULL"
		}
	]
}
~~~

<!-- <span style="color:blue">some *This is Blue italic.* text</span> -->