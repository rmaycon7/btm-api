const Jasmine = require('jasmine')
jasmine = new Jasmine()
jasmine.loadConfigFile('spec/support/jasmine.json')

// import app from "./src/app";
// import chalk from "chalk";

const app = require("../src/app/app"),
	os = require("os"),
	{
		exec
	} = require('child_process')

// const chalk = require("chalk");
let {
	nextAvailable
} = require('node-port-check');

const http = require("http");



// setInterval(() => {
//     http.get("https://btm-api.herokuapp.com");
// }, 126000000); 

// console.log();

const port = () => process.env.PORT || 8080;


nextAvailable(port(), '0.0.0.0').then((next) => {
	let tp = Object.entries(os.networkInterfaces())
	let ips = ""
	for (let i = 0; i < tp.length; i++) {

		ips += "http://" + tp[i][1][0].address + ":" + next
		if (i < tp.length - 1) {
			ips += ", "
		}
	}
	exec('npm run build', (error, out, outerr) => {
		if (!error) {

			let a = app.listen(next, () => {
				// console.log(port());
				// console.clear();
				console.log('running ' + ips)
				jasmine.execute()
				// console.log("\x1b[36m%s\x1b[0m", "I am cyan");
				// console.log(chalk.red("\t\tWellcome To API resources"));

				// console.log(chalk.green("\tServer running in port " + next + "."));
			});
			// setTimeout(() => {
			// 	console.log({
			// 		a: a
			// 	})
			// }, 5000)
		}
	})
})

