# This file is a template, and might need editing before it works on your project.
FROM node:latest

# WORKDIR /usr/src/app

# ARG NODE_ENV
# ENV NODE_ENV $NODE_ENV
# COPY package.json /usr/src/app/
# RUN npm install && npm cache clean
# COPY . /usr/src/app

# CMD [ "npm", "start" ]

# # replace this with your application's default port
# EXPOSE 8080

MAINTAINER Maycon Rebordão <mayconrebordao1122@gmail.com>
# FROM node:8
RUN mkdir -p /usr/src/app
# Create app directory
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# COPY package*.json ./
COPY . /usr/src/app
run npm i -g pnpm 
RUN pnpm add
RUN pnpm run bb
# RUN pnpm run build
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
# COPY . .


EXPOSE 8080

USER mayconrebordao1122@gmail.com

# ENTRYPOINT ["/api-btm"]
# CMD [ "npm", "run", "start-s" ]
CMD [ "pnpm", "start" ]