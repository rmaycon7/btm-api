const jwt = require("jsonwebtoken"),
    authConfig = require("../../config/auth.json"),
    User = require('../models/User'),
    responses = require('../controllers/responses.js'),
    errors = require('../../config/errors.json')

module.exports = async (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        // console.log("error")
        return responses.sendError(res, errors.not_authorized)
        // return res.status(401).send({
        //     error: "The request has not been applied because it lacks valid authentication credentials for the target resource. No token provided."
        // });
    }

    const parts = authHeader.split(" ");

    if (!parts.length === 2) {
        errors.headers_not_found.headers = ['Authorization: Bearer "Token"']
        return responses.sendError(res, errors.headers_not_found)
        // return res.status(412).send({
        //     error: "One or more conditions given in the request header fields evaluated to false when tested on the server. Token format invalid or Token not fount in headers."
        // });
    }

    const [scheme, token] = parts;

    if (!/^Bearer$/i.test(scheme)) {
        errors.headers_not_found.headers = ['Authorization: Bearer "Token"']
        return responses.sendError(res, errors.headers_not_found)
    }

    jwt.verify(token, authConfig.secret, (error, decoded) => {
        if (error) {
            return responses.sendError(res, errors.not_authorized)
            // return res
            //     .status(412)
            //     .send({
            //         error: "One or more conditions given in the request header fields evaluated to false when tested on the server. Token invalid or token not fund in headers."
            //     });
        } else {

            req.userId = decoded.id;
            User.findOne({
                _id: req.userId
            }, (error, user) => {
                if (user) {

                    // console.log('next()')
                    next();
                } else {
                    return responses.sendError(res, errors.user_is_gone)
                    // return res
                    //     .status(404)
                    //     .send({
                    //         error: "User is gone, token no more valid"
                    //     })
                }
            })
        }

        // console.log(req.userId);
    });
};