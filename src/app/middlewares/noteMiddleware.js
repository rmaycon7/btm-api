const Note = require('../models/Note');
const utils = require('../controllers/utils');

// Verifica se os dados da tarefa estão corretos
exports.errorNote = async (req, res, next) => {
    try {

        // Recebe os resultados de validação do utils
        var resulNote = utils.validacaoNote(req.body.name, req.body.content);

        //verifica se a variavel possui alguma coisa
        if (resulNote.length > 0) {
            return res.status(415).send({ //Retorna os erros
                error: resulNote
            });
        } else {
            return next();
        }

    } catch (error) {

        return res.status(500).send({
            error: "Internal server error, please try again."
        })

    }
}