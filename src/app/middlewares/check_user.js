const responses = require('../controllers/responses'),
	errors = require('../../config/errors.json')
module.exports = async (req, res, next) => {
	if (req.params.userId.toString() === req.userId.toString()) {
		next()
	} else {
		return responses.sendError(res, errors.unauthorized)
	}
}