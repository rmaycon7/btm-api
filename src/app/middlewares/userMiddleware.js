const User = require('../models/User');
const utils = require('../controllers/utils');

// Verifica se os dados do usuário estão corretos
exports.errorUser = async (req, res, next) => {
    try {
        // Recebe os resultados de validação do utils
        var resulUser = utils.validacaoUser(req.body.name, req.body.email, req.body.password);
        
        //verifica se a variavel possui alguma coisa
        if (resulUser.length > 0) {
            return res.status(415).send({ //Retorna os erros
                error: resulUser 
            });
        } else {
            return next();
        }
        
    } catch (error) {        
        return res.status(500).send({
            error: "Internal server error, please try again."
        })
    }
}