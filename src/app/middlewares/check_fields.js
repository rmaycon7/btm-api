const errors = require('../../config/errors'),
	responses = require("../controllers/responses")

exports.checkUser = async (req, res, next) => {
	if (req.body.email === undefined || req.body.password === undefined || req.body.name == undefined) {
		// console.log(errors.fields_not_found);
		errors.fields_not_found.fields = ['email', 'name', 'password']
		// console.log(errors.fields_not_found);
		return responses.sendError(res, errors.fields_not_found)
		// return res.status(428).send({
		//     error: "Password and E-mail can not be null."
		// });
	} else {
		return next()
	}
}

exports.checkFields = async (req, res, next) => {
	if (req.body.description === undefined || req.body.name === undefined) {
		// console.log(errors.fields_not_found);
		errors.fields_not_found.fields = ['description', 'name']
		// console.log(errors.fields_not_found);
		return responses.sendError(res, errors.fields_not_found)
		// return res.status(428).send({
		//     error: "Password and E-mail can not be null."
		// });
	} else {
		return next()
	}
}

exports.checkNote = async (req, res, next) => {
	if (req.body.content == undefined ||  req.body.name === undefined ) {
		// console.log(errors.fields_not_found);
		errors.fields_not_found.fields = ['content', 'name']
		// console.log(errors.fields_not_found);
		return responses.sendError(res, errors.fields_not_found)
		// return res.status(428).send({
		//     error: "Password and E-mail can not be null."
		// });
	} else {
		return next()
	}
}