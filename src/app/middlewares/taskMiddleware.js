const Group = require('../models/Task');
const utils = require('../controllers/utils');

// Verifica se os dados da tarefa estão corretos
exports.errorTask = async (req, res, next) => {
    try {

        // Recebe os resultados de validação do utils
        var resulTask = utils.validacaoTask(req.body.name, req.body.description, req.body.private);

        //verifica se a variavel possui alguma coisa
        if (resulTask.length > 0) {
            return res.status(415).send({ //Retorna os erros
                error: resulTask
            });
        } else {
            return next();
        }

    } catch (error) {
        console.log(error);
        
        return res.status(500).send({
            error: "Internal server error, please try again."
        })

    }
}