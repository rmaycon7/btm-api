const Group = require('../models/Group'),
	Note = require('../models/Note'),
	responses = require('../controllers/responses'),
	errors = require('../../config/errors')
const utils = require('../controllers/utils');

// verificando se o grupo existe
exports.groupExists = async (req, res, next) => {

	try {
		/* tentando verificr se o grupo existe, caso exista o usuário procede em sua requisição */
		Group.findOne({
			_id: req.params.groupId
		}, (error, group) => {
			// console.log(req.params.groupId);
			if (group) {

				req.groupId = req.params.groupId
				return next()
			} else {
				return responses.sendError(res, errors.group_not_found)
				// return res.status(404).send({
				// 	error: "Group Not Found, please check group Id and try again."
				// })
			}
		})
	} catch (error) {
		return responses.sendError(res, errors.internal_error)
		// return res.status(500).send({
		// 	error: "Internal server error, please try again."
		// })
	}

}

exports.checkAuth = async (req, res, next) => {
	try {
		Group.findOne({
			_id: req.params.groupId
		}, async (error, group) => {
			if (group) {
				if (group.owner.toString() === req.userId.toString()) {
					req.groupId = req.params.groupId
					return next()
				} else {
					return responses.sendError(res, errors.unauthorized)
				}
			} else {
				return responses.sendError(res, errors.group_not_found)
			}
		})
	} catch (e) {
		// statements
		return responses.sendError(res, errors.internal_error)
		console.log(e);
	}
}

exports.checkAuthNote = async (req, res, next) => {
	try {
		let owner = false
		Group.findOne({
			_id: req.params.groupId
		}, async (error, group) => {
			if (group) {
				if (group.owner.toString() === req.userId.toString()) {
					owner = true
				}
				Note.findOne({
					_id: req.params.noteId
				}, (error, note) => {
					if (note) {
						if (note.owner.toString() === req.userId.toString() || owner) {
							req.groupId = req.params.groupId
							return next()
						} else {
							return responses.sendError(res, errors.unauthorized)
						}
					} else {
						return responses.sendError(res, errors.note_not_found)
					}
				})
			} else {
				return responses.sendError(res, errors.group_not_found)
			}
		})

	} catch (e) {
		// statements
		return responses.sendError(res, errors.internal_error)
		console.log(e);
	}

}

exports.checkAuthNoteOwner = async (req, res, next) => {
	try {
		let owner = false
		Group.findOne({
			_id: req.params.groupId
		}, async (error, group) => {
			if (group) {
				if (group.owner.toString() === req.userId.toString()) {
					owner = true
				}
				Note.findOne({
					_id: req.params.noteId
				}, (error, note) => {
					if (note) {
						if (note.owner.toString() === req.userId.toString() ) {
							req.groupId = req.params.groupId
							return next()
						} else {
							return responses.sendError(res, errors.unauthorized)
						}
					} else {
						console.log('sda')
						return responses.sendError(res, errors.note_not_found)
					}
				})
			} else {
				return responses.sendError(res, errors.group_not_found)
			}
		})

	} catch (e) {
		// statements
		return responses.sendError(res, errors.internal_error)
		console.log(e);
	}

}

// Verifica se os dados do grupo estão corretos
exports.errorGroup = async (req, res, next) => {
	try {

		// Recebe os resultados de validação do utils
		var resulGroup = utils.validacaoGroup(req.body.name, req.body.description);

		//verifica se a variavel possui alguma coisa
		if (resulGroup.length > 0) {
			return res.status(415).send({ //Retorna os erros
				error: resulGroup
			});
		} else {
			return next();
		}

	} catch (error) {

		return res.status(500).send({
			error: "Internal server error, please try again."
		})

	}
}
