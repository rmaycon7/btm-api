const Category = require('../models/Category'),
    responses = require('../controllers/responses.js'),
    errors = require('../../config/errors.json')

const utils = require('../controllers/utils');

// verificando se o grupo existe
exports.categoryExists = async (req, res, next) => {

    try {
        /* tentando verificr se o grupo existe, caso exista o usuário procede em sua requisição */
        Category.findById(req.params.categoryId, (error, category) => {
            // return next()

            if (category) {
                // console.log('teste');

                req.categoryId = req.params.categoryId
                return next()
            } else {
                return responses.sendError(res, errors.category_not_found)
                // return res.status(404).send({
                //     error: "Category Not Found, please check category Id and try again."
                // })
            }
        })
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
        // return res.status(500).send({
        //     error: "Internal server error, please try again."
        // })
    }

}

// Verifica se os dados do grupo estão corretos
exports.errorCategory = async (req, res, next) => {
    try {

        // Recebe os resultados de validação do utils
        var resulCategory = utils.validacaoCategory(req.body.name, req.body.description);

        //verifica se a variavel possui alguma coisa
        if (resulCategory.length > 0) {
            return res.status(402).send({ //Retorna os erros
                error: resulCategory
            });
        } else {
            return next();
        }

    } catch (error) {

        console.log(error);
        

        return res.status(500).send({
            error: "Internal server error, please try again."
        })

    }
}