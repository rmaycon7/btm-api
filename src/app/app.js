const express = require("express"),
	bp = require("body-parser"),
	app = express(),
	compression = require('compression'),
	path = require('path'),
	responses = require('./controllers/responses'),
	errors = require('../config/errors.json'),
	os = require('os'),
	authConfig = require('../config/auth.json'),
	jwt = require('jsonwebtoken'),
	{
		exec
	} = require('child_process'),
	helmet = require('helmet'),
	cors = require('cors')
// client = require('redis').createClient({port: 2288}),
// limiter = require('express-limiter')(app, client)

// RateLimit = require('express-rate-limit'),
// limiter = new RateLimit({
//     windowMs: 5 * 60 * 1000, // 15 minutes 
//     max: 100, // limit each IP to 100 requests per windowMs 
//     delayMs: 0 // disable delaying - full speed until the max limit is reached 
// })

// app.enable('trust proxy'); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS if you use an ELB, custom Nginx setup, etc) 

// app.use(limiter({

// }))
// limiter({
//   path: '*',
//   method: 'all',
//   lookup: ['connection.remoteAddress'],
//   // 150 requests per hour
//   total: 1,
//   expire: 1000 * 60 * 1
// })
app.use(bp.urlencoded({
	extended: true
}));
// usando body-parser para tranformar a requisição em objeto json
app.use(bp.json());
app.use(compression())
app.use(helmet())
// app.use(favicon(path.join(__dirname, '../static', 'icon.svg')))

// app.disable('X-Powered-By')
// app.disable('x-powered-by')
app.disable('x-powered-by');
app.use((req, res, next) => {
	// console.log("T")
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept, Authorization "
	);
	res.header('Access-Control-Allow-Methods', 'GET, PATCH, POST, DELETE, OPTIONS');
	res.header("Content-Type", " application/json; charset=utf-8")
	// res.header('Provided-By', 'btm-api')
	// res.header('X-Powered-By', 'btm-api')
	if (req.method === "OPTIONS")
		res.send();
	else
		next();
	// next();
});

let argv = require('minimist')(process.argv.slice(2));
// console.dir(argv);
let dev = argv['production'] || argv['production']

if (dev) {
	app.use((req, res, next) => {
		// console.log("Error")
		jwt.verify(req.headers['tk'], authConfig.secret, (error, dk) => {
			if (error) {
				return res.status(401).send({
					code: "NOT_RECOGNIZED",
					message: "Erro, cliente não reconhecido",
					name: "Não Permitido.",
					statusCode: 401
				})
			} else {
				next()
			}
		})
	})
}

// console.log("teste")
require("./routes")(app);

// bloco que trata a resposta quando o usuário não encontra nenhuma rota disponivel
app.use(async (req, res, next) => {
	const error = new Error("Recurso não encontrado!")
	error.status = 404;
	//     next(error);
	// });

	// app.use(async (error, req, res, next) => {
	// let e
	res.status(error.status || 500);
	// console.log(error)
	errors.resource_not_found.statusCode = error.status
	errors.resource_not_found.message = error.message
	// console.log({
	//     error: error
	// })
	// e = stdout
	// errors.resource_not_found.host = os.hostname()
	return responses.sendError(res, errors.resource_not_found)
	// await exec('node -v', (error, stdout, stderr) => {
	// })
	// res.json({
	//     error: error.status + "! " + error.message
	// });
});

require('../utils/index.js').wake()

module.exports = app;