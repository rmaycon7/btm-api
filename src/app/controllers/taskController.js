const Task = require("../models/Task"),
    User = require("../models/User"),
    Group = require("../models/Group"),
    CategoryControl = require('./categoryController'),
    responses = require('./responses'),
    errors = require('../../config/errors.json')

exports.getAll = async (req, res, next) => {
    try {
        const query = Task.find({
            belongs_to: req.categoryId
        }).populate(['users'])
        query.exec((error, tasks) => {
            if (error) {
                throw error
            } else {
                let response = tasks.map(task => {
                    task.users = task.users || [];
                    return {
                        id: task._id,
                        name: task.name,
                        category: task.category,
                        description: task.description,
                        deadline: task.deadline,
                        users: task.users.map(user => {
                            return {
                                id: user.id,
                                name: user.name,
                                email: user.email
                            }
                        })
                    };
                });
                if (response.length === 0) {
                    responses.sendError(res, errors.tasks_not_found)

                    // return res.status(202).send({
                    //     message: "No tasks Found."
                    // });
                } else return res.send(response);
            }
        });
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
        // return res.status(500).send({
        //     errot: "Internal server Error, please try again."
        // });
    }
};

exports.getById = async (req, res, next) => {
    try {
        const query = Task.findOne({
            _id: req.params.taskId,
            belongs_to: req.categoryId
        }).populate(['users'])
        query.exec((error, task) => {
            if (!task) {
                return responses.sendError(res, errors.task_not_found)
                // return res.status(404).send({
                //     error: "No tasks found."
                // });
            } else {
                return res.send({
                    id: task._id,
                    name: task.name,
                    category: task.category,
                    description: task.description,
                    deadline: task.deadline,
                    users: task.users.map(user => {
                        return {
                            id: user.id,
                            name: user.name,
                            email: user.email
                        }
                    })
                });
            }
        });
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
};

exports.create = async (req, res, next) => {

    try {
        if (!req.body.name || !req.body.description || req.body.private === undefined) {
            errors.fields_not_found.fields = ['description', 'private', 'name']
            return responses.sendError(res, errors.fields_not_found)
            // return res.status(428).send({
            //     error: "Pre condition failed, please verify request body a d try again. "
            // })
        }
        let {
            name,
            description,
            privaty,
            category,
            deadline,
            users
        } = req.body
        description = description || ""
        users = users || []
        users = users.map(user => {
            return {
                _id: user.userId
            }
        })
        users.push({
            _id: req.userId
        })
        let check = [],
            aux = []
        const grupo = await Group.findOne({
            _id: req.groupId
        })
        for (let i = 0; i < users.length; i++) {
            try {
                // console.log(users[i]);

                if (await User.findOne({
                        _id: [users[i]]
                    }) && grupo.members.find(meb => {
                        return memb.toString() === users[i].toString()
                    }) || grupo.owner.toString() === users[i].toString()) {
                    aux.push(users[i])
                } else {
                    check.push(users[i])
                }

            } catch (error) {

                check.push(users[i])
            }

        }
        users = aux
        // console.log(req.categoryId);
        // return res.send()
        Task.create({
            name,
            description,
            privaty,
            category,
            deadline,
            belongs_to: req.categoryId
        }, async (error, task) => {
            // console.log(task);

            if (task) {
                await Promise.all(users.map(async user => {
                    await task.users.push(user)
                }))
                await task.save()
                await CategoryControl.addIdTaskInCategory(req.categoryId, task.id)
                const query = Task.findById(task._id).populate(['users', 'belongs_to'])
                query.exec(async (error, task) => {
                    if (task) {
                        let response = {
                            id: task._id,
                            name: task.name,
                            category: task.category,
                            description: task.description,
                            deadline: task.deadline,
                            belongs_to: {
                                id: task.belongs_to.id,
                                group_name: task.belongs_to.name,
                                description: task.belongs_to.description
                            },
                            users: task.users.map(user => {
                                return {
                                    id: user.id,
                                    name: user.name,
                                    email: user.email
                                }
                            })
                        };
                        if (check.length !== 0) {
                            response.aviso = {
                                error: "Alguns usuários informados não foram encontrados, segue a lista:",
                                users: check
                            }
                        }
                        return res.status(200).send(response)
                    } else {
                        throw error
                        // return res.status(500).send({
                        //     error: "Internal server error, please try again"
                        // })

                    }
                })
            } else
                // return res.status(500).send({
                //     error: "Internal server error, please try again"
                // })
                throw error
        })
    } catch (error) {
        // console.log(error);
        return responses.sendError(res, errors.internal_error)
        // return res.status(500).send({
        //     errot: "Internal server Error, please try again."
        // });
    }
};

exports.update = async (req, res, next) => {

    try {
        if (!req.body.name || !req.body.description || req.body.private === undefined) {
            errors.fields_not_found.fields = ['name', 'description', 'private']
            return responses.sendError(res, errors.fields_not_found)
            // return res.status(428).send({
            //     error: "Pre condition failed, please verify request body a d try again. "
            // })
        }

        let {
            name,
            description,
            privaty,
            category,
            deadline,
            users
        } = req.body
        description = description || ""

        users = users || []
        users = users.map(user => {
            return {
                _id: user.userId
            }
        })
        users.push({
            _id: req.userId
        })
        let check = [],
            aux = []
        for (let i = 0; i < users.length; i++) {
            try {
                if (await User.findOne({
                        _id: [users[i]]
                    }) && grupo.members.find(meb => {
                        return memb.toString() === users[i].toString()
                    }) || grupo.owner.toString() === users[i].toString()) {
                    aux.push(users[i])
                } else {
                    check.push(users[i])
                }

            } catch (error) {

                check.push(users[i])
            }
        }
        users = aux
        const tarefa = await Task.findOne({
            _id: req.params.taskId,
            belongs_to: req.groupId
        })
        Task.findOneAndUpdate({
                _id: req.params.taskId,
                belongs_to: req.categoryId
            }, {
                name,
                description,
                privaty,
                category,
                deadline,
                belongs_to: req.categoryId
            }, {
                new: true,
                populate: 'belongs_to'
            },
            async (error, task) => {
                if (task) {
                    let aux_members = []
                    task.users = []
                    await Promise.all(users.map(async user => {
                        await task.users.push(user)
                    }))
                    await task.save()
                    const query = Task.findById(task._id).populate(['users', 'belongs_to'])
                    query.exec(async (error, task) => {
                        if (task) {
                            const response = {
                                id: task._id,
                                name: task.name,
                                category: task.category,
                                description: task.description,
                                deadline: task.deadline,
                                belongs_to: {
                                    id: task.belongs_to.id,
                                    group_name: task.belongs_to.name,
                                    description: task.belongs_to.description
                                },
                                users: task.users.map(user => {
                                    return {
                                        id: user.id,
                                        name: user.name,
                                        email: user.email
                                    }
                                })
                            };
                            if (check.length !== 0) {
                                response.aviso = {
                                    error: "Alguns usuários informados não foram encontrados, segue a lista:",
                                    users: check
                                }
                            }
                            return res.status(200).send(response)
                        } else {
                            // return res.status(500).send({
                            //     error: "Internal server error, please try again"
                            // })
                            throw error
                        }
                    })
                } else {
                    return responses.sendError(res, errors.task_not_found)
                    // return res.status(404).send({
                    //     error: "Task Not Found."
                    // })
                }

            })
    } catch (error) {
        console.log(error);
        return responses.sendError(res, errors.internal_error)
        // return res.status(500).send({
        //     errot: "Internal server Error, please try again."
        // });
    }
};

/* removendo tarefa */
exports.remove = async (req, res, next) => {
    try {
        const task = await Task.findOne({
            _id: req.params.taskId,
            belongs_to: req.categoryId
        })
        // console.log(task);

        if (!task) {
            // return res.status(404).send({
            //     error: "Task Not Found."
            // })
            return responses.sendError(res, errors.task_not_found)
        } else {
            // await CategoryControl.removeIdTaskInCategory
            await CategoryControl.removeIdTaskInCategory(req.categoryId, req.params.taskId)
            Task.findOneAndRemove({
                _id: req.params.taskId,
                belongs_to: req.categoryId
            }, (error) => {
                if (!error) {
                    return res.status(202).send({
                        code: "TASK_DELETED",
                        message: "Tarefa deletada com sucesso.",
                        name: "Deletar tarefa",
                        statusCode: 202
                    })
                } else {
                    console.log(error)
                    return responses.sendError(res, errors.task_is_gone)
                }
            })

        }
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
        // console.log(error);

        // return res.status(500).send({
        //     errot: "Task is gone"
        // });
    }
}

exports.removeMore = async (listTask, categoryId) => {
    try {
        // console.log('task');

        await Promise.all(listTask.map(async task => {
            console.log(task);

            try {
                await Task.findOneAndRemove({
                    _id: task,
                    belongs_to: categoryId
                })
            } catch (error) {
                // console.log('task erro');
                // console.log(error);


            }
        }))
        return true
    } catch (error) {
        // console.log('task 2 erro');
        // console.log(error);
        return error
    }
}