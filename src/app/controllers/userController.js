const jwt = require("jsonwebtoken"),
    User = require("../models/User"),
    authConfig = require("../../config/auth.json"),
    GroupControl = require('./groupController'),
    Group = require('../models/Group'),
    responses = require('./responses.js'),
    errors = require('../../config/errors.json'),

    generateToken = (params = {}) => {
        return jwt.sign(params, authConfig.secret, {
            expiresIn: 86400
        })
    },
    mongoose = require('mongoose')


/* rota para listar todos os usuários */
exports.getAll = async (req, res, next) => {
    const query = User.find().sort({
        name: 1
    })
    query.exec(async (error, docs) => {
        if (docs) {
            const users = docs.map(doc => {
                doc.groups = doc.groups || []
                // console.log(docs)
                return {
                    id: doc._id,
                    name: doc.name,
                    email: doc.email
                }
            })
            return res.status(200).send(
                users
            )
        } else {
            // console.log('getAll')
            return responses.sendError(res, errors.users_not_found)
            // return res.status(404).send({
            //     error: "User not found."
            // })
        }
    })
}

/* Rota para listar apenas um usuário */
exports.getById = async (req, res, next) => {
    // statement
    try {
        const query = User.findById(req.params.userId)
        // .select('+ password name email groups')
        // const query = User.findById(req.params.userId).populate("groups").select('+ password name email groups')
        query.exec(async (error, doc) => {
            if (doc) {
                // console.log(doc)
                doc.groups = doc.groups || []
                const user = {
                    id: doc._id,
                    name: doc.name,
                    email: doc.email
                }
                return res.send(
                    user
                )
            } else {
                // console.log('getById')            
                return responses.sendError(res, errors.user_not_found)

                // return res.status(404).send({
                //     error: "User not found."
                // })
            }
        })
    } catch (e) {
        // statements
        console.log(e);
    }
}

exports.create = async (req, res, next) => {
    try {
        const {
            email
        } = req.body

        /* verificando se os dados de cadastro do usário não estão vazios */
        if (!req.body.name || !req.body.password || !req.body.email) {
            errors.fields_not_found.fields = ['name', 'email', 'password']
            responses.sendError(res, errors.fields_not_found)
            // return res.status(428).send({
            //     error: "Email, password or name  is null, but can not be null."
            // })
        } else {

            /* verificando se o email ja esta cadastrado no sistema, ou seja, se o usuário ja existe */
            if (await User.findOne({
                    email: email
                })) {
                responses.sendError(res, errors.user_already_exists)
            } else {

                /* criando usuário */
                User.create(req.body, (error, user) => {
                    if (error) {
                        console.log({
                            error: error
                        })
                        throw error
                    } else {
                        // console.log("send user data")


                        const response = {
                            id: user._id,
                            name: user.name,
                            email: user.email,
                            token: generateToken({
                                id: user._id
                            })
                        }
                        return res.status(200).send(
                            response
                        )
                    }
                })
            }
        }

    } catch (error) {
        console.log({
            error: error
        })
        responses.sendError(res, errors.internal_error)
    }
}

/* Rota para atualizar usuários */
exports.update = async (req, res, next) => {
    try {
        const {
            name,
            password,
            email
        } = req.body
        if (!req.body.name || !req.body.password || !req.body.email) {
            errors.fields_not_found.fields = ['name', 'email', 'password']
            responses.sendError(res, errors.fields_not_found)
            // return res.status(428).send({
            //     error: "Email, password or name  is null, but can not be null."
            // })
        } else {
            const data = await User.findById(req.params.userId)
            // console.log({
            //     data_f: data.id
            // })

            let update = async () => {
                User.findOneAndUpdate({
                    _id: req.params.userId
                }, {
                    user_id: req.params.userId,
                    name,
                    password,
                    email
                }, {
                    new: true
                }, async (error, user) => {
                    if (user) {
                        return res.send({
                            id: user._id,
                            name: user.name,
                            email: user.email
                        })
                    } else {
                        return responses.sendError(res, errors.internal_error)
                    }
                })
            }
            if (data) {
                User.findOne({
                    _id: req.params.userId
                }, async (error, user) => {
                    if (!user) {
                        responses.sendError(res, errors.user_not_found)
                    } else {

                        // , {
                        //             user_id: req.params.userId,
                        //             name,
                        //             password,
                        //             email
                        //         }

                        // const tmp = await User.findOne({_id: req.params.userId})
                        // console.log({tmp:tmp})
                        if (user.email != email) {

                            User.findOne({
                                    email: email,
                                }, {
                                    new: true
                                },
                                async (error, user) => {
                                    console.log({
                                        error: error
                                    })
                                    if (user) {
                                        responses.sendError(res, errors.user_already_exists)
                                    } else {
                                        // await user.save()

                                        update()
                                    }
                                }
                            )
                        } else {
                            update()
                            // responses.sendError(res, errors.user_already_exists)
                        }



                    }
                })

            }
        }

    } catch (error) {
        responses.sendError(res, errors.internal_error)
    }
}

exports.delete = async (req, res, next) => {
    try {
        const user = await User.findOne({
            _id: req.params.userId
        })
        await Group.find({
            owner: req.params.userId
        }, async (error, owners) => {
            owners = owners || []
            // console.log(owners)

            await Promise.all(owners.map(async owner => {
                await GroupControl.removeGroup(owner.id)
            }))
        })

        await Group.find({
            members: {
                $in: [req.userId]
            }
        }, async (error, members) => {
            members = members || []
            await Promise.all(members.map(async member => {
                member.members = member.members.filter(memb => {
                    // console.log(member.members)


                    return memb.toString() !== req.userId.toString()
                })
                await Group.findOneAndUpdate({
                    _id: member.id
                }, member, {
                    new: true
                })
            }))
        })

        const useraux = await User.findByIdAndRemove(req.params.userId)
        // console.log(user)

        if (!useraux) {
            return responses.sendError(res, errors.user_not_found)
            // return res.status(404).send({
            //     message: "Cannot delete user, because he's gone."
            // })
        } else {

            const response = {
                statusCode: 202,
                name: "Deletar usuário",
                message: "User delete successfull.",
                code: "USER_DELETED"
            }
            return res.status(202).send(response)
        }
    } catch (error) {
        console.log(error)
        return responses.sendError(res, errors.internal_error)

        // return res.status(500).send({
        //     message: "Internal error.Cannot delete user, please Try again."
        // })
    }

}


/* método para adicionar a referência de um grupo a um usuário */
exports.addIdGroupInUsers = async (groupId, users) => {
    try {
        for (let i = 0; i < users.length; i++) {
            const user = await User.findById(users[i]).select('password groups name email invitations')
            if (user.groups.length === 0) {
                user.groups.push(groupId)
            } else {
                let check = false
                /* verificando se o id do grupo ja existe na lista de grupos do usuário */
                for (let j = 0; j < user.groups.length; j++) {
                    if (user.groups[j] === groupId)
                        check = true

                }
                if (!check) {
                    await user.groups.push(groupId)
                }
            }

            const {
                name,
                email,
                password,
                groups,
                invitations
            } = user
            // console.log({addGroup: users[i]})
            const q = User.findOneAndUpdate({
                _id: users[i]
            }, {
                user_id: user.id,
                name,
                email,
                password,
                groups,
                invitations
            }, {
                new: true
            })
            q.exec()

        }
        return true
    } catch (error) {
        return error
    }
}

/* método para remover a referência de um grupo de um usuário */
exports.removeIdGroupInUsers = async (groupId, users) => {
    try {
        for (let i = 0; i < users.length; i++) {
            const user = await User.findById(users[i]).select('password name groups email invitations')
            let tasks = []
            /* removendo apenas um grupo da lista de grupos do usuários */
            user.groups = user.groups.filter(group => {
                return group.toString() !== groupId.toString()
            })

            const {
                name,
                email,
                password,
                groups,
                invitations
            } = user
            await User.findOneAndUpdate({
                _id: users[i]
            }, {
                user_id: user.id,
                name,
                email,
                password,
                groups,
                invitations
            }, {
                new: true
            })
        }
        return true
    } catch (error) {
        return error
    }
}

/* método para adicionar a convite de um grupo a um usuário */
exports.addIdGroupinvitations = async (req, data) => {
    try {
        let inserts = {}
        inserts.users = []
        const user = await User.findOne({
            email: data.email
        }).select('password name email groups invitations')
        if (user) {
            if (user.id === req.userId.toString()) {
                inserts.user.push({
                    user: data.email,
                    message: "Esta se sentindo sozinho? Não pode mandar convite pra você mesmo.",
                    statusCode: 409
                })
            } else {


                user.invitations = user.invitations || []
                // console.log({user:users})
                if (user.groups.find(gp => {
                        console.log(gp, req.groupId)
                        return gp.toString() === req.groupId.toString()
                    })) {
                    inserts.users.push({
                        user: data.email,
                        message: "Usuário ja faz parte do grupo.",
                        statusCode: 202,
                        name: "Enviar convite para entrar em um grupo para um usuário."
                    })
                } else if (user.invitations.length === 0) {
                    // console.log('c')
                    user.invitations.push(req.groupId)
                } else {
                    let check = false
                    // console.log(groupId)

                    /* verificando se o id do grupo ja existe na lista de grupos do usuário */
                    for (let j = 0; j < user.invitations.length; j++) {
                        // console.log(user.invitations[j])

                        if (user.invitations[j].toString() === req.groupId.toString())
                            check = true

                    }
                    // console.log(check)

                    if (!check) {
                        await user.invitations.push(req.groupId)
                    }
                }
                const {

                    name,
                    email,
                    password,
                    invitations,
                    groups
                } = user
                await User.findOneAndUpdate({
                    _id: user.id
                }, {
                    user_id: user.id,
                    name,
                    email,
                    password,
                    invitations,
                    groups
                }, {
                    new: true
                })
            }

        } else {

            inserts.users.push({
                user: data.email,
                message: "Usuário não encontrado",
                statusCode: 404
            })

        }
        // console.log(users)
        inserts.ok = true
        return inserts
    } catch (error) {
        console.log(error)
        inserts.ok = false
        inserts.error = error
        return inserts
    }
}

/* método para remover a convite de um grupo de um usuário */
exports.removeIdGroupinvitations = async (req, user) => {
    try {

        const useraux = await User.findById(user).select('name email groups invitations password ')

        /* removendo apenas um grupo da lista de grupos do usuários */
        useraux.invitations = useraux.invitations.filter(group => {
            return group.toString() !== req.groupId.toString()
        })

        /* removendo apenas um grupo da lista de convites do usuários */
        useraux.groups = useraux.groups.filter(group => {
            // console.log(group.toString(), req.groupId.toString())
            return group.toString() !== req.groupId.toString()
        })
        // console.log(useraux)
        const {
            name,
            email,
            password,
            invitations,
            groups
        } = useraux
        await User.findOneAndUpdate({
            _id: user
        }, {
            user_id: useraux.id,
            name,
            email,
            password,
            invitations,
            groups
        }, {
            new: true
        }, (error, user) => {
            if (error) {
                console.log(error)

            }
            // console.log({user: user})
        })
        return true
    } catch (error) {
        return error
    }
}


exports.getWeekDays = async (req, res, next) => {
    try {
        User.find(async (error, users) => {
            // console.log(users)
            let days = [{

                name: 'Sunday',
                count: 0

            }, {

                name: 'Monday',
                count: 0

            }, {

                name: 'Tuesday',
                count: 0

            }, {

                name: 'Wednesday',
                count: 0

            }, {

                name: 'Thursday',
                count: 0

            }, {

                name: 'Friday',
                count: 0

            }, {

                name: 'Saturday',
                count: 0

            }]
            users.map(doc => {
                // console.log({doc: doc})
                let a = new Date(doc.createAt)
                try {
                    days[a.getDay()].count++
                        // console.log(days[a.getDay()])

                } catch (error) {}
                // console.log({days: days[a.getDay()]})
            })
            return res.send(days)
        })
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}


exports.seelAllInvitations = async (req, res, next) => {
    try {

        /*
    Vizualizar todos os convites que o usuário possui 
     */
        // console.log('seeAll')
        // console.log(req.userId)
        const query = User.findOne({
            _id: req.userId
        }, (error, user) => {
            // console.log(user)
            if (user) {

                return res.send({
                    invitations: user.invitations.map(invite => {
                        return {
                            id: invite.id,
                            name: invite.name,
                            description: invite.description
                        }
                    })
                })
            } else if (error) {
                throw error
            } else {
                return responses.sendError(res, errors.invitations_not_found)
            }
        }).select('invitations').populate('invitations')
        query.exec()

    } catch (error) {
        responses.sendError(res, errors.internal_error)
    }
}

exports.confirmInvitation = async (req, res, next) => {
    try {
        // console.log('confirmInvitation')
        const query = User.findOne({
            _id: req.userId
        }, async (error, user) => {
            if (user) {
                const {
                    name,
                    email,
                    password,
                    groups,
                    invitations
                } = user
                // console.log(user)
                // console.log({
                //     password: password
                // })
                // console.log({
                //     invitations: invitations
                // })
                if (invitations.find(invite => {
                        return invite.toString() === req.params.inviteId.toString()
                    })) {
                    // console.log('accpeting ....')
                    // console.log({password: password})
                    let a = req.params.inviteId + ""

                    // console.log( mongoose.Types.ObjectId(a))
                    if (await GroupControl.addIdUserInGroup(req.params.inviteId, req.userId)) {
                        groups.push(mongoose.Types.ObjectId(a))
                        const update = User.findOneAndUpdate({
                            _id: req.userId
                        }, {
                            user_id: req.userId,
                            name,
                            password,
                            email,
                            groups,
                            invitations: invitations.filter(invite => {
                                return invite.toString() !== req.params.inviteId.toString()
                            })
                        }, {
                            new: true
                        }, (error, user) => {
                            // console.log({password})
                            // console.log(user)
                            // console.log('ok')
                            if (user) {
                                return res.send({
                                    code: "INVITE_ACCEPT",
                                    message: "Convite aceito com sucesso.",
                                    name: "Convite aceito",
                                    statusCode: 202
                                })
                            } else {
                                throw error
                            }
                        })
                        // const update = User.findOneAndUpdate({
                        //         _id: req.userId
                        //     }, {
                        //         name,
                        //         email,
                        //         password,
                        //         groups: groups.push(req.params.inviteId),
                        //         invitations: invitations.filter(invite => {
                        //                 return invite.toString() !== req.params.inviteId.toString()
                        //             }
                        //         )},
                        //         {
                        //             new: true
                        //         },
                        //         (error, user) => {
                        //             console.log('ok')
                        //             if (user) {
                        //                 return res.send({
                        //                     code: "INVITE_ACCEPT",
                        //                     message: "Convite aceito con sucesso.",
                        //                     name: "Convite aceito",
                        //                     statusCode: 202
                        //                 })
                        //             } else {
                        //                 throw error
                        //             }

                        //         })
                        // })
                        update.exec()
                    } else {
                        return response.sendError(res, errors.groups_not_found)
                    }

                } else {
                    return responses.sendError(res, errors.invitation_not_found)
                }
            } else if (error) {
                throw error
            } else {
                return responses.sendError(res, errors.invitation_not_found)
            }
        }).select('name password email groups invitations')
        query.exec()
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }

}

exports.recuseInvitation = async (req, res, next) => {
    try {
        // console.log('confirmInvitation')
        const query = User.findOne({
            _id: req.userId
        }, (error, user) => {
            if (user) {
                const {
                    name,
                    email,
                    password,
                    groups,
                    invitations
                } = user
                if (invitations.find(invite => {
                        return invite.toString() === req.params.inviteId.toString()
                    })) {
                    // console.log('accpeting ....')
                    // console.log({password: password})
                    let a = req.params.inviteId + ""

                    // console.log( mongoose.Types.ObjectId(a))
                    // groups.push(mongoose.Types.ObjectId(a))
                    const update = User.findOneAndUpdate({
                        _id: req.userId
                    }, {
                        user_id: req.userId,
                        name,
                        password,
                        email,
                        groups,
                        invitations: invitations.filter(invite => {
                            return invite.toString() !== req.params.inviteId.toString()
                        })
                    }, {
                        new: true
                    }, (error, user) => {

                        if (user) {
                            return res.send({
                                code: "INVITE_REFUSED",
                                message: "Convite recusado com sucesso.",
                                name: "Convite recusado",
                                statusCode: 202
                            })
                        } else {
                            throw error
                        }
                    })

                    update.exec()
                } else {
                    return responses.sendError(res, errors.invitation_not_found)
                }
            } else if (error) {
                throw error
            } else {
                return responses.sendError(res, errors.invitation_not_found)
            }
        }).select('name password email groups invitations')
        query.exec()
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }

}


exports.outGroup = async (req, res, next) => {
    try {
        const query = User.findOne({
            _id: req.userId
        }, async (error, user) => {
            if (user) {
                Group.findOne({
                    $and: [

                        {
                            _id: req.params.groupId
                        }, {
                            $or: [{
                                members: {
                                    $in: [req.userId]
                                }
                            }, {
                                owner: req.userId
                            }]

                        },


                    ]
                }, async (error, group) => {
                    if (group) {
                        if (group.owner.toString() === req.userId.toString()) {
                            return res.send({
                                code: "BAD_REQUEST",
                                message: "O dono do grupo não pode sair do grupo, pode apenas deletar o grupo, mesmo efeito, mas todos o usário seram removidos.",
                                name: "Não permitido",
                                statusCode: 400
                            })
                        } else {
                            if (await GroupControl.removeIdUserInGroup(req.params.groupId, req.userId)) {
                                let {
                                    name,
                                    email,
                                    password,
                                    groups,
                                    invitations
                                } = user
                                User.findOneAndUpdate({
                                    _id: req.userId
                                }, {
                                    user_id: req.userId,
                                    name,
                                    email,
                                    password,
                                    groups: groups.filter(gp => {
                                        return dp.toString() !== req.params.groupId.toString()
                                    }),
                                    invitations
                                }, {
                                    new: true
                                }, (error, doc) => {
                                    if (doc) {
                                        return res.send({
                                            code: "OUT_SUCCESSFULL",
                                            message: "O usuário saiu do grupo com sucesso.",
                                            name: "Saiu do grupo",
                                            statusCode: 202
                                        })
                                    } else {
                                        // console.log({
                                        //     arq: "aqui"
                                        // })
                                        throw error
                                    }
                                })
                            } else {
                                return responses.sendError(res, errors.internal_error)
                            }
                        }
                    } else {
                        return responses.sendError(res, errors.group_not_found)
                        // throw error
                    }
                })
            } else {
                return responses.sendError(res, errors.user_not_found)
            }
        }).select('password name email groups invitations')
        query.exec()
        // statements
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
        // statements
        // console.log(error);
    }
}