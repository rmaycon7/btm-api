const Group = require('../models/Group');
const validator = require('validator');

exports.ServerError = async (res) => {
    return res.status(500).send({
        error: "Internal server error, plese try again."
    })
}

// Retirar os espaços de nome e descrição
const RetiraEspaco = (data) => {
    let offspace = data;
    offspace = offspace.split(" ").join("");

    return offspace;
}

// Função que valida o nome e retorna o valor das validações --errorMsg--
const ValidName = (name) => {
    
    let nameSpace = RetiraEspaco(name);
    
    let validNome = validator.isAlphanumeric(nameSpace); // Verifica se o nome só possui letras
    let validNomeNull = validator.isEmpty(name); // Verifica se o nome está vazio
    let errorMsg = "" // Mensagem de erro

    /* verificando se o nome usário não está vazio */
    if (validNomeNull) {
        errorMsg= {
            statusCode: 428,
            name: "error",
            message: "O campo nome está vazio",
            code: "NAME_NULL"
        } ;
    } else {
        /* verificando se o nome do usuario so possui letras*/
        if (!validNome) {
            errorMsg = { 
                statusCode: 415,
                name: "error",
                message: "O nome é invalida",
                code: "NAME_INVALID",
                teste: nameSpace
            };
        }
    }

    return errorMsg; // retorna a mensagem de erro --errorMsg--
}

// Função que valida o email e retorna o valor das validações --errorMsg--
const ValidEmail = (email) => {
    let validEmail = validator.isEmail(email); // Verifica se é um email valido
    let validEmailNull = validator.isEmpty(email); // Verifica se o email está vazio
    let errorMsg = ""; // Mensagem de erro

    /* Verifica se o email é null */
    if (validEmailNull) {
        errorMsg = { 
            statusCode: 428,
            name: "error",
            message: "O campo email está vazio",
            code: "EMAIL_NULL" 
        };
    } else {
        /* Verifica se o email é valido */
        if (!validEmail) {
            errorMsg = { 
                statusCode: 415,
                name: "error",
                message: "O email é invalida",
                code: "EMAIL_INVALID"
            }
        }
    }

    return errorMsg; // retorna a mensagem de erro --errorMsg--

}

// Função que valida a senha e retorna o valor das validações --errorMsg--
const ValidPassword = (password) => {
    let validPasswordLenght = validator.isByteLength(password, { min: 3, max: undefined }); // Verifica o tamanho da senha
    let validPasswordNull = validator.isEmpty(password); // Verifica se a senha está vazia
    let validPassword = validator.isAlphanumeric(password); // Verifica se a senha é valida
    let errorMsg = ""; // Mensagem de erro

    if (validPasswordNull) {
        errorMsg = { 
            statusCode: 428,
            name: "error",
            message: "O campo da senha está vazio",
            code: "PASSWORD_NULL"
        }; 
    } else {
        if (!validPasswordLenght) {
            errorMsg = { 
                statusCode: 422,
                name: "error",
                message: "A senha é muito curta",
                code: "PASSWORD_SMALL"
            }; 
        } else{
            if (!validPassword) {
                errorMsg = { 
                    statusCode: 415,
                    name: "error",
                    message: "A senha é invalida",
                    code: "PASSWORD_INVALID"
                };
            }
        }
    }

    return errorMsg; // retorna a mensagem de erro --errorMsg--

}

// Função que valida o valor de privacidade do grupo e retorna o valor das validações --errorMsg--
const ValidPrivate = (privaties) => {
    let validPrivate = validator.isBoolean(privaties); // Verifica se é true ou false
    let validPrivateNull = validator.isEmpty(privaties); // verifica se está vazio
    let errorMsg = ""; // Menagem de erro

    /* Verifica se o atributo private é null */
    if (validPrivateNull) {
        errorMsg = { 
            statusCode: 428,
            name: "error",
            message: "O campo private está vazio",
            code: "PRIVATE_NULL"
        };
    } else {
        /* Verifica se o atributo private é valido */
        if (!validPrivate) {
            errorMsg = { 
                statusCode: 415,
                name: "error",
                message: "O Private é invalida",
                code: "PRIVATE_INVALID"
            };
        }
    }

    return errorMsg; // retorna a mensagem de erro --errorMsg--

}

// Função que valida o valor da descrição do grupo e retorna o valor das validações --errorMsg--
const ValidDescription = (description) => {
    let errorMsg = "";
    let validDescriptionEmpty = validator.isEmpty(description); // Verifica se está vazio
    
    let descriptionSpace = RetiraEspaco(description);

    let validDescription = validator.isAlphanumeric(descriptionSpace); // Verifica se a descrição so possui letras e números

    if (description === "") {
        // Se a descrição for vazia não faz nada ( regra de negocio )
    }else {
        if ( !validDescription ) {
            errorMsg = { 
                statusCode: 415,
                name: "error",
                message: "A descrição é invalida",
                code: "DESCRIPTION_INVALID"
            };
        }
    }

    return errorMsg; // retorna a mensagem de erro --errorMsg--

}

// Função que valida o valor da descrição do grupo e retorna o valor das validações --errorMsg--
const ValidContent = (content) => {
    let errorMsg = "";
    let validContentEmpty = validator.isEmpty(content);

    if (validContentEmpty) {
        errorMsg = { 
            statusCode: 428,
            name: "error",
            message: "O campo nome está vazio",
            code: "CONTENT_NULL" 
        }
    }

    return errorMsg;
}

// Função que verifica se alguma validação retornou algum erro e junta as msgs de erros em um array --msg--
/* Como os dados se repetem em letios controllers utilizei uma função so, para que não precisa repetir codigo,
deste modo os ifs fazem as verificação e inserem as mensagens somente se a letiavel --errorMsgDado-- não estiver
vazia */
const ErrorLenght = (errorMsgName, errorMsgEmail, errorMsgPassword, errorMsgDescription, errorMsgPrivate, errorMsgContent) => {
    let msg = new Array(); // Array das mensagens de erros 
    
    if (errorMsgName != "") {
        msg.push(errorMsgName);
    }

    if (errorMsgEmail != "") {
        msg.push(errorMsgEmail);
    }

    if (errorMsgPassword != "") {
        msg.push(errorMsgPassword);
    }

    if (errorMsgDescription != "") {
        msg.push(errorMsgDescription);
    }

    if (errorMsgPrivate != "") {
        msg.push(errorMsgPrivate);
    }

    if (errorMsgContent != "") {
        msg.push(errorMsgContent);
    }   

    return msg;
}

// Função de validação dos dados do usuário, utiliza as funções anteriores e retorna todos os erros --verificResul--
exports.validacaoUser = (name, email, password) => { 

    let priv_name = ValidName(name);
    let priv_email = ValidEmail(email);
    let priv_password = ValidPassword(password);
    let verificResul = ErrorLenght(priv_name, priv_email, priv_password, "", "", ""); // É preciso passar a string vazia "" para os
                                                                   // dados que não forem utilizados
    return verificResul; // Retorna as mensagens

}

// Função de validação dos dados do grupo, utiliza as funções anteriores e retorna todos os erros --verificResul--
exports.validacaoGroup = (name, description) => {

    let priv_name = ValidName(name);
    let priv_description = ValidDescription(description);
    let verificResul = ErrorLenght(priv_name, "", "", priv_description, "", ""); // É preciso passar a string vazia "" para os
                                                                   // dados que não forem utilizados
    return verificResul; // Retorna as mensagens

}

// Função de validação dos dados da tarefa, utiliza as funções anteriores e retorna todos os erros --verificResul--
exports.validacaoTask = (name, description, privaties) => {

    let priv_name = ValidName(name);
    let priv_description = ValidDescription(description);
    let priv_privaties = ValidPrivate(privaties);
    let verificResul = ErrorLenght(priv_name, "", "", priv_description, priv_privaties, ""); // É preciso passar a string vazia "" para os
                                                                        // dados que não forem utilizados
    return verificResul; // Retorna as mensagens
    
}

// Função de validação dos dados da categoria, utiliza as funções anteriores e retorna todos os erros --verificResul--
exports.validacaoCategory = (name, description) => {
    let priv_name = ValidName(name);
    let priv_description = ValidDescription(description);
    let verificResul = ErrorLenght(priv_name, "", "", priv_description, "","");

    return verificResul;
}

// Função de validação dos dados da nota, utiliza as funções anteriores e retorna todos os erros --verificResul--
exports.validacaoNote = (name, content) => {
    let priv_name = ValidName(name);
    let priv_content = ValidContent(content);
    let verificResul = ErrorLenght(priv_name, "", "", "", "", priv_content)
}
