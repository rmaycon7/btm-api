const bcrypt = require("bcryptjs"),
    jwt = require("jsonwebtoken"),
    User = require("../models/User"),
    responses = require('./responses'),
    authConfig = require("../../config/auth"),
    crypto = require("crypto"),
    errors = require('../../config/errors.json')


// função para gerar o token do usuário
const generateToken = (params = {}) => {
    return jwt.sign(params, authConfig.secret, {
        expiresIn: 86400
    });
}

exports.authenticate = async (req, res, next) => {
    const {
        email,
        password
    } = req.body;
    // verificando se os campos de senha e email não estão vazios
    try {
        if (!email || !password) {
            // console.log(errors.fields_not_found);
            errors.fields_not_found.fields = ['email', 'password']
            // console.log(errors.fields_not_found);
            return responses.sendError(res, errors.fields_not_found)
            // return res.status(428).send({
            //     error: "Password and E-mail can not be null."
            // });
        } else {
            // consulta para verificar se o usuário existe no sistema atraves do email
            let query = User.findOne({
                email
            }).select("+ password name email");
            query.exec(async (error, user) => {
                // console.log()
                if (!user) {
                    return responses.sendError(res, errors.user_not_found)
                    // return res.status(404).send({
                    //     error: "User not found."
                    // });
                } else {

                    /* verificando se as senha que esta no sistema é compativel com a informada poeloo usuário */
                    /*
                    , (err, hash) =>{
                        console.log({error: err, hash: hash})
                    }
                    */
                    if (!(await bcrypt.compare(password, user.password))) {
                        // console.log(password)
                        // console.log("teste")
                        return responses.sendError(res, errors.password_wrong)
                        // return res.status(401).send({
                        //     error: "Password not match."
                        // });
                    } else {

                        // console.log(user)
                        return res.status(201).send({
                            id: user.id,
                            name: user.name,
                            email: user.email,
                            token: generateToken({
                                id: user._id
                            })
                        });
                    }
                }
            });

        }
    } catch (e) {
        // statements
         return responses.sendError(res, errros.internal_error)
        console.log(e);
    }

}