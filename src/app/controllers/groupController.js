const Group = require("../models/Group"),
    User = require("../models/User"),
    UserControl = require("./userController"),
    CategoryControl = require("./categoryController"),
    NoteControl = require("./noteController"),
    responses = require('./responses'),
    errors = require('../../config/errors.json')

/* métodos privados */
const ownerVerify = (owner, user) => {
    // console.log(owner, user)
    return owner.toString() === user.toString();
};
/* método pra pegar todos os grupos que o usúario participa ou é dono */
exports.getAll = async (req, res, next) => {
    try {
        const query = Group.find({
            $or: [{
                members: {
                    $in: [req.userId]
                }
            }, {
                owner: req.userId
            }]
            // }).populate(["categories", "members", "notes"]);
        })
        // }).populate(["categories", "members", {path: "notes", model: "Note" , populate: { path: "owner", model: "User"}}]);
        // 
        query.exec(async (error, groups) => {
            // console.log(error)
            /* é retornado um erro caso o grupo não seja encontrado, esse erro é repassado ao usuário com um 404 . */
            if (error) {
                return responses.sendError(res, errors.groups_not_found)
                // return res.status(404).send({
                //     error: "Group Not found."
                // });
            }
            let response = groups.map(group => {
                group.categories = group.categories || [];
                // return res.send(groups)
                return {
                    id: group._id,
                    name: group.name,
                    description: group.description,
                    isOwner: ownerVerify(group.owner, req.userId),
                }
            });
            if (response.length === 0) {
                return responses.sendError(res, errors.groups_not_found)
                // return res.status(202).send({
                //     message: "No groups found."
                // });
            } else {
                return res.send(response);
            }
        });
    } catch (error) {
        /* informa o usuário da api que houve um error interno no servidor */
        return responses.sendError(res, errors.internal_error)
    }
};

exports.getById = async (req, res, next) => {
    try {
        // await console.log(
        //     Group.findById(req.params.groupId)
        // );
        // console.clear()
        // console.log(req.params.groupId);
        // await Group.findOne({
        //     _id: req.params.groupId
        // }, (error, group) => {
        //     console.log(group);

        // })

        // console.log({
        //     _id: req.params.groupId
        // });

        const query = Group.findOne({
            $and: [

                {
                    _id: req.params.groupId
                }, {
                    $or: [{
                        members: {
                            $in: [req.userId]
                        }
                    }, {
                        owner: req.userId
                    }]

                },


            ]

        })
        // }).populate(["members", "categories", 'notes']);
        query.exec(async (error, group) => {
            /* é retornado um erro caso o grupo não seja encontrado, esse erro é repassado ao usuário com um 404 . */
            // console.clear()
            // console.log(group);

            if (!group) {
                return responses.sendError(res, errors.group_not_found)
                // return res.status(404).send({
                //     error: "Group Not found."
                // });
            } else {
                /*  verificando se o valor de categories é unfined, caso seja ele recebe um vetor vazio */
                group.categories = group.categories || [];

                /* retornando dados do grupo */
                return res.status(200).send({
                    id: group._id,
                    name: group.name,
                    description: group.description,
                    isOwner: ownerVerify(group.owner, req.userId)

                });
            }
        });
    } catch (error) {
        // console.log(error);

        /* informa o usuário da api que houve um error interno no servidor */
        return responses.sendError(res, errors.internal_error)

        // return res.status(500).send({
        //     error: "Internal  error, plaese try again."
        // });
    }
}



exports.getById2 = async (req, res, next) => {
    try {
        // await console.log(
        //     Group.findById(req.params.groupId)
        // );
        // console.clear()
        // console.log(req.params.groupId);
        // await Group.findOne({
        //     _id: req.params.groupId
        // }, (error, group) => {
        //     console.log(group);

        // })

        // console.log({
        //     _id: req.params.groupId
        // });

        const query = Group.findOne({
            $and: [

                {
                    _id: req.params.groupId
                }, {
                    $or: [{
                        members: {
                            $in: [req.userId]
                        }
                    }, {
                        owner: req.userId
                    }]

                },


            ]

        }).populate([{
            path: "members",
            model: "User"
        }, {
            path: "categories",
            model: "Category",
            populate: {
                path: "tasks",
                model: "Task"
            }
        }, {
            path: "notes",
            model: "Note"
        }])
        // }).populate(["members", "categories", 'notes']);
        query.exec(async (error, group) => {
            /* é retornado um erro caso o grupo não seja encontrado, esse erro é repassado ao usuário com um 404 . */
            // console.clear()
            // console.log(group);

            if (!group) {
                return responses.sendError(res, errors.group_not_found)
                // return res.status(404).send({
                //     error: "Group Not found."
                // });
            } else {
                /*  verificando se o valor de categories é unfined, caso seja ele recebe um vetor vazio */
                group.categories = group.categories || [];
                // let members,categories, tasks
                // if(group.members[0].id !==undefined){

                // }
                /* retornando dados do grupo */
                return res.status(200).send({
                    id: group._id,
                    name: group.name,
                    description: group.description,
                    isOwner: ownerVerify(group.owner, req.userId),
                    members: group.members.map(memb => {

                        return {
                            id: memb.id,
                            name: memb.name,
                            email: memb.email
                        }
                    }),
                    categories: group.categories.map(cat => {
                        return {
                            id: cat.id,
                            name: cat.name,
                            description: cat.description,
                            tasks: cat.tasks.map(tk => {
                                return {
                                    id: tk.id,
                                    name: tk.name,
                                    description: tk.description
                                }
                            })
                        }
                    }),
                    notes: group.notes.map(note => {
                        return {
                            id: note.id,
                            name: note.name,
                            content: note.content
                        }
                    })

                });
            }
        });
    } catch (error) {
        // console.log(error);

        /* informa o usuário da api que houve um error interno no servidor */
        return responses.sendError(res, errors.internal_error)

        // return res.status(500).send({
        //     error: "Internal  error, plaese try again."
        // });
    }
}
exports.create = async (req, res, next) => {
    try {
        if (!req.body.name)
            return res.status(428).send({
                error: "Name cannot be null. "
            });
        else {
            let {
                name,
                description
            } = req.body;
            description = description || "";
            Group.create({
                    name,
                    description,
                    owner: req.userId
                },
                async (error, group) => {
                    if (error) {
                        throw error
                    } else {
                        await UserControl.addIdGroupInUsers(
                            group.id, [req.userId]
                        );
                        const query = Group.findOne({
                            _id: group.id
                        })
                        // .populate(
                        //     "owner"
                        // );
                        // console.log(group);

                        query.exec((error, query_group) => {
                            if (query_group) {

                                // console.log(query_group);

                                return res.send({
                                    id: query_group.id,
                                    name: query_group.name,
                                    description: query_group.description,
                                    isOwner: true
                                });
                            }
                        });
                    }
                }
            );
        }
    } catch (error) {
        /* informa o usuário da api que houve um error interno no servidor */
        return responses.sendError(res, errors.internal_error)

    }
};

exports.update = async (req, res, next) => {
    try {
        /* verificando se o nome do grupo é fornecido na requisição */
        if (!req.body.name) {

            /* avisa que o nome não pode ser nulo/vazio */
            errors.fields_not_found.fields = ['name']
            return responses.sendError(res, errors.fields_not_found)

            // return res.status(428).send({
            //     error: "Name cannot be null. "
            // });
        } else {
            let {
                name,
                description
            } = req.body;
            description = description || "";
            const grupo = await Group.findOne({
                _id: req.params.groupId,
                $or: [{
                    members: {
                        $in: [req.userId]
                    }
                }, {
                    owner: req.userId
                }]
            }).populate("owner");
            if (!grupo) {
                console.log(grupo)
                return responses.sendError(res, errors.group_not_found)

            } else {
                console.log(grupo.owner.id);

                if (ownerVerify(grupo.owner.id, req.userId)) {
                    Group.findOneAndUpdate({
                            _id: req.params.groupId,
                            $or: [{
                                members: {
                                    $in: [req.userId]
                                }
                            }, {
                                owner: req.userId
                            }]
                        }, {
                            name,
                            description,
                            owner: req.userId
                        }, {
                            new: true
                        },
                        async (error, group) => {
                            if (!group) {
                                console.log(error);

                                throw error
                            } else {
                                const query = Group.findById(group.id)
                                // .populate(
                                //     ["members", "owner"]
                                // );
                                query.exec((error, query_group) => {
                                    if (query_group)
                                        return res.send({
                                            id: query_group.id,
                                            name: query_group.name,
                                            description: query_group.description,
                                            isOwner: true
                                            // owner: {
                                            //     id: query_group.owner._id,
                                            //     name: query_group.owner.name,
                                            //     email: query_group.owner.email
                                            // },
                                            // members: query_group.members.map(
                                            //     member => {
                                            //         return {
                                            //             id: member.id,
                                            //             name: member.name,
                                            //             email: member.email
                                            //         };
                                            //     }
                                            // )
                                        });
                                });
                            }
                        }
                    );
                } else {
                    return responses.sendError(res, errors.not_authorized)

                    // return res.status(401).send({
                    //     error: "Você não esta autorizado a deletar este grupo, apenas o administrador pode deleta-lo."
                    // });
                }
            }
        }
    } catch (error) {
        /* informa o usuário da api que houve um error interno no servidor */
        // console.log(error);
        return responses.sendError(res, errors.internal_error)

        // return res.status(500).send({
        //     error: "Internal  error, plaese try again."
        // });
    }
};

/* rota para deletar um groupo */
exports.delete = async (req, res, next) => {
    try {
        const group = await Group.findOne({
            _id: req.params.groupId,
            $or: [{
                members: {
                    $in: [req.userId]
                }
            }, {
                owner: req.userId
            }]
        });
        if (!group) {
            return responses.sendError(res, errors.group_not_found)

            // return res.status(410).send({
            //     error: "Group not found, please try again."
            // });
        } else {
            await UserControl.removeIdGroupInUsers(group.id, group.members);
            await CategoryControl.removeMore(group.categories, req.groupId);
            await NoteControl.removeMore(group.notes, req.groupId);
            Group.findOneAndRemove({
                    _id: req.params.groupId,
                    $or: [{
                        members: {
                            $in: [req.userId]
                        }
                    }, {
                        owner: req.userId
                    }]
                },
                async (error, group) => {
                    /* verificando se o grupo foi encontrado, caso não seja encontrado significa que o grupo não existe mais ou nunca existiu, por isso é retornado um 410. */
                    if (error) {
                        return responses.sendError(res, errors.group_not_found)

                        // return res.status(410).send({
                        //     error: "Group not found, please try again."
                        // });
                    }
                    /* caso não esta erro,  ele foi deletado com sucesso, é retornado uma messagem e um 202. */
                    else {
                        await UserControl.removeIdGroupInUsers(group.id, [req.userId]);
                        return res.status(202).send({
                            code: "GROUP_DELETED",
                            message: "Grupo deletado com sucesso.",
                            name: "Deletar Grupo",
                            statusCode: 202
                        });
                    }
                }
            );
        }
    } catch (error) {
        /* informa o usuário da api que houve um error interno no servidor */
        return responses.sendError(res, errors.internal_error)

    }
};

/* método para adicionar a referência de uma tarefa a um grupo */
exports.addIdCategoryInGroup = async (groupId, categoryId) => {
    try {
        const group = await Group.findById(groupId);
        if (!group.categories.find(category => {
                return category === categoryId;
            })) {
            await group.categories.push(categoryId);
            let {
                name,
                description,
                members,
                categories,
                notes
            } = group;
            await Group.findByIdAndUpdate(groupId, {
                name,
                description,
                members,
                categories,
                notes
            });
        }
        return true;
    } catch (error) {
        return false;
    }
};

/* método para remover a referência de uma tarefa de um grupo */
exports.removeIdCategoryInGroup = async (groupId, categoryId) => {
    try {
        const group = await Group.findById(groupId);
        group.categories = group.categories.filter(category => {
            return category.toString() !== categoryId.toString();
        });
        let {
            name,
            description,
            members,
            categories,
            notes
        } = group;
        await Group.findByIdAndUpdate(groupId, {
            name,
            description,
            members,
            categories,
            notes
        });
        return true;
    } catch (error) {
        return false;
    }
};



/* método para adicionar a referência de uma tarefa a um grupo */
exports.addIdNoteInGroup = async (groupId, noteId) => {
    try {
        const group = await Group.findById(groupId);
        if (!group.notes.find(note => {
                return note === noteId;
            })) {
            await group.notes.push(noteId);
            let {
                name,
                description,
                members,
                categories,
                notes
            } = group;
            await Group.findByIdAndUpdate(groupId, {
                name,
                description,
                members,
                categories,
                notes
            });
        }
        return true;
    } catch (error) {
        return false;
    }
};

/* método para remover a referência de uma tarefa de um grupo */
exports.removeIdNoteInGroup = async (groupId, noteId) => {
    try {
        const group = await Group.findById(groupId);
        group.notes = group.notes.filter(note => {
            return note.toString() !== noteId.toString();
        });
        let {
            name,
            description,
            members,
            categories,
            notes
        } = group;
        await Group.findByIdAndUpdate(groupId, {
            name,
            description,
            members,
            categories,
            notes
        });
        return true;
    } catch (error) {
        return false;
    }
};



/* método para adicionar a referência de uma tarefa a um grupo */
exports.addIdUserInGroup = async (groupId, userId) => {
    try {
        const group = await Group.findById(groupId);
        if (!group.members.find(member => {
                return member.toString() === userId.toString()
            })) {
            await group.members.push(userId);
            let {
                name,
                description,
                members,
                categories,
                notes
            } = group;
            await Group.findByIdAndUpdate(groupId, {
                name,
                description,
                members,
                categories,
                notes
            });
        }
        return true;
    } catch (error) {
        return false;
    }
};

/* método para remover a referência de uma usuário de um grupo */
exports.removeIdUserInGroup = async (groupId, userId) => {
    try {
        const group = await Group.findById(groupId);
        console.log(group)
        group.members = group.members.filter(member => {
            return member.toString() !== userId.toString();
        });
        let {
            name,
            description,
            members,
            categories,
            notes
        } = group;
        console.log({
            groupo: {
                name,
                description,
                members,
                categories,
                notes
            }
        })
        await Group.findOneAndUpdate({
            _id: groupId
        }, {
            name,
            description,
            members,
            categories,
            notes
        });
        return true;
    } catch (error) {
        // console.log({
        //     error: error
        // })
        return false;
    }
};


exports.removeGroup = async (groupId) => {
    try {
        console.log({
            grouId: groupId
        });
        Group.findOne({
            _id: groupId
        }, async (error, group) => {
            console.log(group);

            await UserControl.removeIdGroupInUsers(group.id, group.members);
            await CategoryControl.removeMore(group.categories, groupId);
            await NoteControl.removeMore(group.notes, groupId);
            await Group.findOneAndRemove({
                _id: groupId
            })
            return true
        })
    } catch (error) {
        return false
    }
}


exports.getWeekDays = async (req, res, next) => {
    try {
        Group.find(async (error, groups) => {
            // console.log(groups)
            let days = [{

                name: 'Sunday',
                count: 0

            }, {

                name: 'Monday',
                count: 0

            }, {

                name: 'Tuesday',
                count: 0

            }, {

                name: 'Wednesday',
                count: 0

            }, {

                name: 'Thursday',
                count: 0

            }, {

                name: 'Friday',
                count: 0

            }, {

                name: 'Saturday',
                count: 0

            }]
            groups.map(doc => {
                // console.log({doc: doc})
                let a = new Date(doc.createAt)
                try {
                    days[a.getDay()].count++
                        // console.log(days[a.getDay()])

                } catch (error) {}
                // console.log({days: days[a.getDay()]})
            })
            return res.send(days)
        })
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}