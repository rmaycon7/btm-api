const Note = require('../models/Note'),
    GroupControl = require('./groupController'),
    responses = require('./responses.js'),
    errors = require('../../config/errors.json'),
    /* métodos privados */
    ownerVerify = (owner, user) => {
        return owner.toString() === user.toString();
    }
exports.getAll = async (req, res, next) => {
    try {
        const query = Note.find({
            belongs_to: req.groupId
        }, (error, notes) => {
            // console.log(notes)
            if (notes.length !== 0) {

                return res.send(notes.map(note => {
                    return {
                        id: note.id,
                        name: note.name,
                        content: note.content,
                        owner: {
                            email: note.owner.email,
                            name: note.owner.name
                        }
                    }
                }))
            } else {
                return responses.sendError(res, errors.notes_not_found)
            }
        }).populate(['owner'])
        query.exec()
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}

exports.getById = async (req, res, next) => {
    try {
        const query = Note.findOne({
            _id: req.params.noteId,
            belongs_to: req.groupId
        })
        console.clear()
        await Note.findOne({
            _id: req.params.noteId
        }, (error, note) => {
            if (note) {
                return res.send({
                    id: note.id,
                    name: note.name,
                    content: note.content,
                    owner: {
                        email: note.owner.email,
                        name: note.owner.name
                    }
                })
            } else {
                throw error
            }
        }).populate(['owner'])

        query.exec()
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}
exports.create = async (req, res, next) => {
    try {
        let {
            name,
            content
        } = req.body
        if (!name || !content) {
            errors.fields_not_found.fields = ['name', 'content']
            return responses.sendError(res, errors.fields_not_found)
            // return res.satus(401).send({
            //     error: "Name or content cannot be null. Please check this and try again."
            // })
        } else {
            Note.create({
                name,
                content,
                belongs_to: req.groupId,
                owner: req.userId
            }, async (error, note) => {
                if (error) {
                    throw error
                } else {
                    await GroupControl.addIdNoteInGroup(req.groupId, note._id)
                    const query = Note.findOne({
                        _id: note.id
                    }, (error, note) => {
                        return res.send({
                            id: note.id,
                            name: note.name,
                            content: note.content
                        })
                    }).populate(['owner'])
                    query.exec()
                }
            })
        }
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}
exports.update = async (req, res, next) => {
    try {
        let {
            name,
            content
        } = req.body
        if (!name || !content) {
            errors.fields_not_found.fields = ['name', 'content']
            return responses.sendError(res, errors.fields_not_found)
            // return res.satus(401).send({
            //     error: "Name or content cannot be null. Please check this and try again."
            // })
        } else {
            Note.findOneAndUpdate({
                _id: req.params.noteId,
                belongs_to: req.groupId
            }, {
                name,
                content,
                belongs_to: req.groupId,
                owner: req.userId
            }, {
                new: true
            }, async (error, note) => {
                if (error) {
                    return responses.sendError(res, errors.note_not_found)
                } else {
                    const query = Note.findOne({
                        _id: req.params.noteId,
                        belongs_to: req.groupId
                    }, (error, note) => {
                        if (error) {
                            return responses.sendError(res, errors.note_not_found)
                        } else {

                            return res.send({
                                id: note.id,
                                name: note.name,
                                content: note.content
                            })
                        }
                    }).populate(['owner'])
                    query.exec()
                }
            })
        }
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}
exports.delete = async (req, res, next) => {
    try {
        Note.findOne({
            _id: req.params.noteId,
            belongs_to: req.params.groupId
        }, async (error, note) => {
            if (note) {
                if (ownerVerify(req.userId, note.owner)) {
                    Note.findOneAndRemove({
                        _id: req.params.noteId,
                        belongs_to: req.groupId
                    }, async (error) => {
                        if (!error) {
                            await GroupControl.removeIdNoteInGroup(req.groupId, req.params.noteId)

                            return res.send({
                                code: "NOTE_DELETED",
                                message: "Nota deletada com sucesso.",
                                name: "Nota Deletada",
                                statusCode: 202
                            })
                        } else {
                            throw error
                        }
                    })
                } else {
                    return responses.sendError(res, errors.not_authorized)
                }

            } else {
                return responses.sendError(res, errors.note_not_found)
            }
        })

    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}



exports.removeMore = async (noteList, groupId) => {
    try {
        await Promise.all(noteList.map(async note => {
            await Note.findOneAndRemove({
                _id: note,
                belongs_to: groupId
            })
        }))
        return true
    } catch (error) {
        return false
    }
}