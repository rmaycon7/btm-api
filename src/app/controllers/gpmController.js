const Group = require("../models/Group"),
    UserControl = require('./userController'),
    responses = require('./responses'),
    errors = require('../../config/errors.json')


exports.getAll = async (req, res, next) => {
    try {
        const query = Group.findOne({
            _id: req.groupId,
            $or: [{
                members: {
                    $in: [req.userId]
                }
            }, {
                owner: req.userId
            }]
        }, (error, members) => {
            // console.log(members)
            members.members = members.members || []
            if (members && members.members.length !== 0) {
                return res.send({
                    members: members.members.map(member => {
                        return {
                            id: member.id,
                            name: member.name,
                            email: member.email
                        }
                    })
                })
            } else {
                // throw error
                return responses.sendError(res, errors.no_members_found)
                // return res.status(202).send({
                //     error: "No members Found."
                // })
            }
        }).select('members').populate('members')
        query.exec()
    } catch (error) {
        // console.log(error)
        return responses.sendError(res, errors.internal_error)
        // return utils.ServerError(res)
    }
}

exports.add = async (req, res, next) => {
    try {
        const group = await Group.find({

            owner: req.userId

        })
        if (group) {
            // console.clear()
            // console.log('teste');

            let data = await UserControl.addIdGroupinvitations(req, req.body),
                response = {
                    code: "INVETE_SEND",
                    message: "Usuário(s) convidado(s) com sucessso.",
                    name: "Convite Efetuado",
                    statusCode: 202,

                }
            if (data.ok) {
                if (data.users.length !== 0) {
                    response.code = "INVITE_NOT_SEND"
                    response.name = data.users[0].name
                    response.statusCode = data.users[0].statusCode
                    response.message = data.users[0].message
                    // response.warnning = data.users
                }
                return responses.sendError(res, response)
            } else {
                throw error
                // return responses.sendError(res, errors.internal_error)

                // return res.status(500).send({
                //     error: "erro"
                // })
            }

        } else {
            return responses.sendError(res, errors.group_not_found)
        }
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
        // return utils.ServerError(res)
    }
}

exports.rm = async (req, res, next) => {
    try {
        const group = await Group.findOne({
            _id: req.groupId
        })
        if (group) {
            // console.log(group)
            // console.log('teste');
            // console.log(group.members)
            if (await UserControl.removeIdGroupinvitations(req, req.params.memberId) || await UserControl.removeIdGroupInUsers(req.groupId, req.params.memberId)) {
                Group.findOneAndUpdate({
                    _id: group.id
                }, {
                    name: group.name,
                    description: group.description,
                    members: group.members.filter(memb => {
                        // console.log(memb.toString(), req.params.memberId.toString())
                        return memb.toString() !== req.params.memberId.toString()
                    }),
                    categories: group.categories,
                    notes: group.notes,
                }, {
                    new: true
                }, (error, group) => {
                    if (!error) {
                        // statement
                        return res.send({
                            code: "MEMBER_REMOVED",
                            message: "Membro removido com sucessso.",
                            name: "Remover membro do grupo.",
                            statusCode: 202
                        })
                    } else {
                        console.log({error: error})
                        throw error
                    }
                })
            } else {
                console.log(error)
                return responses.sendError(res, errors.internal_error)
                // return res.status(500).send({
                //     error: "erro"
                // })
            }

        }

    } catch (error) {
        console.log(error)
        return responses.sendError(res, errors.internal_error)
        // return utils.ServerError(res)
    }
}