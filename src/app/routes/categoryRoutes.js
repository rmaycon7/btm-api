const express = require("express"),
	categoryControl = require("../controllers/categoryController"),
	groupMiddle = require('../middlewares/groupMiddleware'),
	router = express.Router(),
	auth = require('../middlewares/auth'),
	categoryMiddleware = require('../middlewares/categoryMiddleware'),
	checkFields = require("../middlewares/check_fields.js")

router.use(auth)

router
	.get('/:groupId/categories/', groupMiddle.groupExists, categoryControl.getAll)
	.get('/:groupId/categories/:categoryId', groupMiddle.groupExists, categoryControl.getById)
	.post('/:groupId/categories/', groupMiddle.checkAuth, checkFields.checkFields, categoryMiddleware.errorCategory, categoryControl.create)
	.patch('/:groupId/categories/:categoryId', groupMiddle.checkAuth, checkFields.checkFields, categoryMiddleware.errorCategory, categoryControl.update)
	.delete('/:groupId/categories/:categoryId', groupMiddle.checkAuth, categoryControl.remove)

module.exports = app => app.use("/groups", router);