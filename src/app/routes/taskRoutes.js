const taskControl = require("../controllers/taskController"),
	categoryMiddle = require('../middlewares/categoryMiddleware'),
	groupMiddle = require('../middlewares/groupMiddleware'),
	express = require("express"),
	router = express.Router(),
	auth = require('../middlewares/auth'),
	checkFields = require("../middlewares/check_fields.js")

router.use(auth)

// groupMiddle.groupExists()
router
	.get("/:groupId/categories/:categoryId/tasks/", groupMiddle.groupExists, categoryMiddle.categoryExists, taskControl.getAll)
	.get("/:groupId/categories/:categoryId/tasks/:taskId", groupMiddle.groupExists, categoryMiddle.categoryExists, taskControl.getById)
	.post("/:groupId/categories/:categoryId/tasks/", groupMiddle.checkAuth,checkFields.checkFields, categoryMiddle.categoryExists, categoryMiddle.errorCategory, taskControl.create)
	.patch("/:groupId/categories/:categoryId/tasks/:taskId", groupMiddle.checkAuth,checkFields.checkFields, categoryMiddle.categoryExists, categoryMiddle.errorCategory, taskControl.update)
	.delete("/:groupId/categories/:categoryId/tasks/:taskId", groupMiddle.checkAuth, categoryMiddle.categoryExists, taskControl.remove)
// module.exports = app => app.use("/task", router);

module.exports = app => app.use("/groups/", router);