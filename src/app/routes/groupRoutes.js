const express = require("express"),
	router = express.Router(),
	groupControl = require("../controllers/groupController"),
	gpmControl = require("../controllers/gpmController"),
	taskControl = require("../controllers/taskController"),
	auth = require("../middlewares/auth"),
	groupMiddle = require('../middlewares/groupMiddleware'),
	checkFields = require("../middlewares/check_fields.js")
// taskControl.

router.use(auth);

// Rotas exclusivas dos grupos
router
	.get("/", groupControl.getAll)
	.get("/:groupId", groupControl.getById)
	.get("/:groupId/all", groupControl.getById2)
	.post("/", checkFields.checkFields, groupMiddle.errorGroup, groupControl.create)
	.patch("/:groupId", checkFields.checkFields, groupMiddle.checkAuth, groupMiddle.errorGroup, groupControl.update)
	.delete("/:groupId", groupMiddle.checkAuth, groupControl.delete)
	.get("/:groupId/members", groupMiddle.checkAuth, gpmControl.getAll)
	.post("/:groupId/members", groupMiddle.checkAuth, gpmControl.add)
	.delete("/:groupId/members/:memberId", groupMiddle.checkAuth, gpmControl.rm)

// rotas de das tarefas dos grupos
// router.get("/:groupId/tasks/", taskControl.getAll);
// router.get("/:groupId/tasks/:taskId", taskControl.getById);
// router.post("/:groupId/tasks/", taskControl.create);
// router.patch("/:groupId/tasks/:taskId", taskControl.update);

module.exports = app => app.use("/groups", router);