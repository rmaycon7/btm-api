'use-strict'
const express = require('express'),
	router = express.Router(),
	authenticate = require('../controllers/authController'),
	userControl = require('../controllers/userController'),
	groupControl = require('../controllers/groupController'),
	auth = require('../middlewares/auth.js')

router
	.post('/authenticate', authenticate.authenticate)
	.post('/register', userControl.create)
	.get('/report/users/weekdays', auth, userControl.getWeekDays)
	.get('/report/groups/weekdays', auth, groupControl.getWeekDays)
// .get('/report/groups/weekdays', groupControl.getWeekDays)


module.exports = app => app.use('/', router)