const userControl = require("../controllers/userController"),
	express = require("express"),
	router = express.Router(),
	auth = require("../middlewares/auth"),
	teste = require('../controllers/groupController.js'),
	check = require('../middlewares/check_user.js'),
	checkFields = require("../middlewares/check_fields.js")


const userMiddleware = require("../middlewares/userMiddleware");

// router.use(auth);
// teste.
// usando o middleware auth para verificar se o usuário esta registrado/logado na aplicação
router
	.get("/", auth, userControl.getAll)
	.get('/invitations', auth, userControl.seelAllInvitations)
	.post("/invitations/:inviteId", auth, userControl.confirmInvitation)
	.delete("/invitations/:inviteId", auth, userControl.recuseInvitation)
	.get("/:userId", auth, check, userControl.getById)
	.post("/", checkFields.checkUser, userMiddleware.errorUser, userControl.create)
	.patch("/:userId", auth, checkFields.checkUser, check, userMiddleware.errorUser, userControl.update)
	.delete("/:userId", auth, check, userControl.delete)
	.delete('/group/:groupId', auth, userControl.outGroup)

module.exports = app => app.use("/users", router);

// => app.use("/authenticate", router);su