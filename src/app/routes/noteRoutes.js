const noteControl = require("../controllers/noteController"),
	groupMiddle = require('../middlewares/groupMiddleware'),
	express = require("express"),
	router = express.Router(),
	auth = require('../middlewares/auth'),
	checkFields = require("../middlewares/check_fields.js")

router.use(auth)



router
	.get('/:groupId/notes/', groupMiddle.groupExists, noteControl.getAll)
	.get('/:groupId/notes/:noteId', groupMiddle.groupExists, noteControl.getById)
	.post('/:groupId/notes/', groupMiddle.groupExists,checkFields.checkNote, noteControl.create)
	.patch('/:groupId/notes/:noteId', groupMiddle.checkAuthNoteOwner,checkFields.checkNote, noteControl.update)
	.delete('/:groupId/notes/:noteId', groupMiddle.checkAuthNote, noteControl.delete)

module.exports = app => app.use('/groups', router)