// beforeAll
// const server = require('../test/index.js')
const request = require("request"),
	getNum = () => Math.floor((Math.random() * 10000) + 1),
	getData = (data) => {
		if (data != undefined) {
			if (data.body != undefined) {
				data = JSON.parse(data.body)
			} else {
				data = JSON.parse(data)
			}
		} else {
			data = new Error("Error, data is undefined.")
		}

		return data
	},
	getDataNow = () => {
		let now = new Date()
		return "." + now.getFullYear() + "." + now.getMonth() + "." + now.getDay() + "." + now.getHours() + "." + now.getMinutes() + "." + now.getMilliseconds()

	}
// console.log(getData())

const num = getDataNow()
let data = undefined
let config = {
	url: "http://localhost:8080",
	form: {
		name: "Maycon",
		email: "maycon" + num + "@email.com",
		password: "teste" + num
	}
}

describe('Testando as rotas de Configuração BTM', () => {
	it('Teste no register', (done) => {

		request.post({
			url: "http://localhost:8080/register",
			form: {
				name: "Maycon",
				email: "maycon" + num + "@email.com",
				password: "teste"
			}
		}, (error, response, body) => {
			data = getData(response.body)
			// console.log(data)
			// console.log(response.statusCode)
			expect({
				name: data.name
			}).toEqual({
				name: "Maycon"
			})
			done()
			// expect({name: data.name }).toEqual({name: config.name})
		})
		// console.log(getNum())
		// console.log(server)
		// done()



	}, 9000000)
	it('Teste no authenticate', (done) => {
		// data = undefined
		// data = config
		// data.url = data.url.concat("/authenticate")
		// console.log({
		// 	auth: data
		// })
		request.post({
			url: "http://localhost:8080/authenticate",
			form: {
				name: "Maycon",
				email: "maycon" + num + "@email.com",
				password: "teste"
			}
		}, (error, response, body) => {
			data = getData(response.body)
			// console.log(data)
			// console.log(response.statusCode)
			expect({
				name: data.name
			}).toEqual({
				name: "Maycon"
			})
			expect({
				email: data.email
			}).toEqual({
				email: "maycon" + num + "@email.com"
			})
			expect({
				status: response.statusCode
			}).toEqual({
				status: 201
			})
			done()
		})
	}, 9000000)

})