// beforeAll
// const server = require('../test/index.js')
const request = require("request"),
	getNum = () => Math.floor((Math.random() * 10000) + 1),
	getData = (data) => {
		if (data != undefined) {
			if (data.body != undefined) {
				data = JSON.parse(data.body)
			} else {
				data = JSON.parse(data)
			}
		} else {
			data = new Error("Error, data is undefined.")
		}

		return data
	},
	getDataNow = () => {
		let now = new Date()
		return "." + now.getFullYear() + "." + now.getMonth() + "." + now.getDay() + "." + now.getHours() + "." + now.getMinutes() + "." + now.getMilliseconds()

	}
// console.log(getData())

const num = getDataNow()
let data = undefined

describe("Teste das rotas de usuário", () => {
	let token, id
	it("Criando usuário", (done) => {
		request.post({
			url: "http://localhost:8080/users",
			form: {
				name: "Maycon",
				email: "maycon" + num + "@email.com",
				password: "teste"
			}
		}, (error, response, body) => {
			body = getData(response.body)
			// console.log(body)
			token = body.token
			id = body.id
			expect({
				name: body.name,
				email: body.email
			}).toEqual({
				name: "Maycon",
				email: "maycon" + num + "@email.com"
			})
			done()
		})
	},9000000)
	it("Vendo dados usuário", (done) => {
		request.get({
			url: "http://localhost:8080/users/"+id,
			form: {
				name: "Maycon",
				email: "maycon" + num + "@email.com",
				password: "teste"
			},
			headers:{
				'Authorization': "Bearer "+token
			}
		}, (error, response, body) => {
			body = getData(response.body)
			// token = body.token
			// console.log(body)
			expect({
				name: body.name,
				email: body.email
			}).toEqual({
				name: "Maycon",
				email: "maycon" + num + "@email.com"
			})
			done()
		})
	},9000000)

})