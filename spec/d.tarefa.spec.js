// beforeAll
// const server = require('../test/index.js')
let tk = require('../src/config/data.json')

const request = require("request"),
    getNum = () => Math.floor((Math.random() * 10000) + 1),
    getData = (data) => {
        if (data != undefined) {
            if (data.body != undefined) {
                data = JSON.parse(data.body)
            } else {
                data = JSON.parse(data)
            }
        } else {
            data = new Error("Error, data is undefined.")
        }

        return data
    },
    getDataNow = () => {
        let now = new Date()
        return "." + now.getFullYear() + "." + now.getMonth() + "." + now.getDay() + "." + now.getHours() + "." + now.getMinutes() + "." + now.getMilliseconds()

    }
// console.log(getData())

const num = getDataNow()
let data = undefined

describe("Teste das rotas de tarefas", () => {
    let token, id, idDelete
    it("Criando tarefa", (done) => {
        request.post({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/" + tk.categoryId + "/tasks",
            form: {
                name: "Teste de tarefa",
                description: ""
            },
            headers: {
                'Authorization': "Bearer " + tk.token
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // console.log(body)

            id = body.id

            expect({
                name: body.name,
                description: body.description
            }).toEqual({
                name: "Teste de tarefa"
            })
            done()
        })
    }, 9000000)
    it("Vendo dados da tarefa", (done) => {
        request.get({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/" + tk.categoryId + "/tasks/" + id,
            headers: {
                'Authorization': "Bearer " + tk.token
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // token = body.token
            // console.log(body)
            expect({
                name: body.name,
                description: body.description
            }).toEqual({
                name: "Teste de tarefa"
            })
            done()
        })
    }, 9000000)

    it("Criando tarefa para ser deletada", (done) => {
        request.post({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/" + tk.categoryId + "/tasks",
            form: {
                name: "Tarefa" + num,
                description: ""
            },
            headers: {
                'Authorization': "Bearer " + tk.token
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // console.log(body)

            idDelete = body.id

            expect({
                name: body.name,
                description: body.description
            }).toEqual({
                name: "Terefa" + num
            })
            done()
        })
    }, 9000000)

    it("Atualizando dados do tarefa", (done) => {
        request.patch({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/" + tk.categoryId + "/tasks/" + idDelete,
            form: {
                name: "mateus tarefa",
                description: ""
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // console.log(body)
            expect({
                name: body.name,
                description: body.describe
            }).toEqual({
                name: "mateus tarefa"
            })
            done()
        })
    }, 9000000)

    it("Deletando grupo", (done) => {
        request.delete({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/" + tk.categoryId + "/tasks/" + idDelete,
            headers: {
                'Authorization': "Bearer " + tk.token
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // console.log(body)
            expect(202)
            done()
        })
    }, 9000000)
})