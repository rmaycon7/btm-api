// beforeAll
// const server = require('../test/index.js')
let tk = require('../src/config/data.json')

const request = require("request"),
	getNum = () => Math.floor((Math.random() * 10000) + 1),
	getData = (data) => {
		if (data != undefined) {
			if (data.body != undefined) {
				data = JSON.parse(data.body)
			} else {
				data = JSON.parse(data)
			}
		} else {
			data = new Error("Error, data is undefined.")
		}

		return data
	},
	getDataNow = () => {
		let now = new Date()
		return "." + now.getFullYear() + "." + now.getMonth() + "." + now.getDay() + "." + now.getHours() + "." + now.getMinutes() + "." + now.getMilliseconds()

	}
// console.log(getData())

const num = getDataNow()
let data = undefined

describe("Teste das rotas de grupo", () => {
	let token, id, idDelete
	it("Criando grupo", (done) => {
		request.post({
			url: "http://localhost:8080/groups",
			form: {
				name: "Maycon",
				description: ""
			},
			headers: {
				'Authorization': "Bearer " + tk.token
			}
		}, (error, response, body) => {
			body = getData(response.body)
			// console.log(body)
			
			id = body.id

			const fs = require('fs')
			path = require('path')
			let data = require('../src/config/data.json')
			data.groupId = id
			fs.writeFile(path.normalize(path.resolve(__dirname, '../src/config/data.json')), JSON.stringify(data, null, '\t'), function (error) {
				if (error) {
					console.log({ error: error });
				}

			})

			expect({
				name: body.name,
				description: body.description
			}).toEqual({
				name: "Maycon"
			})
			done()
		})
	}, 9000000)
	it("Vendo dados do grupo", (done) => {
		request.get({
			url: "http://localhost:8080/groups/" + id,
			headers: {
				'Authorization': "Bearer " + tk.token
			}
		}, (error, response, body) => {
			body = getData(response.body)
			// token = body.token
			// console.log(body)
			expect({
				name: body.name,
				description: body.description
			}).toEqual({
				name: "Maycon"
			})
			done()
		})
	}, 9000000)

	it("Criando grupo para ser deletado", (done) => {
		request.post({
			url: "http://localhost:8080/groups",
			form: {
				name: "Maycon" + num,
				description: ""
			},
			headers: {
				'Authorization': "Bearer " + tk.token
			}
		}, (error, response, body) => {
			body = getData(response.body)
			// console.log(body)

			idDelete = body.id

			expect({
				name: body.name,
				description: body.description
			}).toEqual({
				name: "Maycon"
			})
			done()
		})
	}, 9000000)

	it("Atualizando dados do grupo", (done) => {
		request.patch({
			url: "http://localhost:8080/groups" + idDelete,
			form: {
				name: "mateus grupo",
				description: ""
			}
		}, (error, response, body) => {
			body = getData(response.body)
			// console.log(body)
			expect({
				name: body.name,
				description: body.describe
			}).toEqual({
				name: "mateus grupo"
			})
			done()
		})
	}, 9000000)

	it("Deletando grupo", (done) => {
		request.delete({
			url: "http://localhost:8080/groups" + idDelete,
			headers: {
				'Authorization': "Bearer " + tk.token
			}
		}, (error, response, body) => {
			body = getData(response.body)
			// console.log(body)
			expect(202)
			done()
		})
	}, 9000000)
})