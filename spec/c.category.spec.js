// beforeAll
// const server = require('../test/index.js')
let tk = require('../src/config/data.json')

const request = require("request"),
    getNum = () => Math.floor((Math.random() * 10000) + 1),
    getData = (data) => {
        if (data != undefined) {
            if (data.body != undefined) {
                data = JSON.parse(data.body)
            } else {
                data = JSON.parse(data)
            }
        } else {
            data = new Error("Error, data is undefined.")
        }

        return data
    },
    getDataNow = () => {
        let now = new Date()
        return "." + now.getFullYear() + "." + now.getMonth() + "." + now.getDay() + "." + now.getHours() + "." + now.getMinutes() + "." + now.getMilliseconds()

    }
// console.log(getData())

const num = getDataNow()
let data = undefined

describe("Teste das rotas de categorias", () => {
    let token, id, idDelete
    it("Criando categoria", (done) => {
        request.post({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories",
            form: {
                name: "Teste de categoria",
                description: ""
            },
            headers: {
                'Authorization': "Bearer " + tk.token
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // console.log(body)

            id = body.id

            const fs = require('fs')
            path = require('path')
            let data = require('../src/config/data.json')
            data.categoryId = id
            fs.writeFile(path.normalize(path.resolve(__dirname, '../src/config/data.json')), JSON.stringify(data, null, '\t'), function (error) {
                if (error) {
                    console.log({ error: error });
                }

            })

            expect({
                name: body.name,
                description: body.description
            }).toEqual({
                name: "Teste de categoria"
            })
            done()
        })
    }, 9000000)
    it("Vendo dados da categoria", (done) => {
        request.get({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/" + id,
            headers: {
                'Authorization': "Bearer " + tk.token
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // token = body.token
            // console.log(body)
            expect({
                name: body.name,
                description: body.description
            }).toEqual({
                name: "Teste de categoria"
            })
            done()
        })
    }, 9000000)

    it("Criando categoria para ser deletada", (done) => {
        request.post({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/",
            form: {
                name: "Categoria" + num,
                description: ""
            },
            headers: {
                'Authorization': "Bearer " + tk.token
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // console.log(body)

            idDelete = body.id

            expect({
                name: body.name,
                description: body.description
            }).toEqual({
                name: "Categoria" + num
            })
            done()
        })
    }, 9000000)

    it("Atualizando dados do tarefa", (done) => {
        request.patch({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/" + idDelete,
            form: {
                name: "mateus categoria",
                description: ""
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // console.log(body)
            expect({
                name: body.name,
                description: body.describe
            }).toEqual({
                name: "mateus categoria"
            })
            done()
        })
    }, 9000000)

    it("Deletando grupo", (done) => {
        request.delete({
            url: "http://localhost:8080/groups/" + tk.groupId + "/categories/" + idDelete,
            headers: {
                'Authorization': "Bearer " + tk.token
            }
        }, (error, response, body) => {
            body = getData(response.body)
            // console.log(body)
            expect(202)
            done()
        })
    }, 9000000)
})